# Group Name
System React


# Group Members
* Sophia Hu
* David Leung
* Marvin Lee
* Jonas Petit-frere


## Design
  Api design - docs\api-design.MD

  ![ALT](/docs/wireframe.png)


## Intended market
  The purpose of this project is to be a travel website that give the user the weather and activities to enjoy when they are planning a trip to a location around the world. The users who would most benefit are people who want to go to a new location but have no clue on what to do, where to start, and how to pack for the occasion.


## Built With
* [![React][React.js]][React-url]
* [![Bootstrap][Bootstrap.com]][Bootstrap-url]
* [![FastAPI][fastApi]][fastApi-url]
* [![Redux][Redux.js]][Redux-url]
* [![Python][Python.org]][Python-url]
* [![JavaScript][Javascript.js]][Javascript-url]


## Functionality
  Main-page: Page used for creating a day trip. It provides the main-landing-page with the necessary data to retrieve information from the APIs.

  Main-landing-page: Page shows all the data in component cards returned from the APIs for the user to add activities(museums, trails, cuisine) to days during the trip.
			Weather component is a card that shows the current weather at the trip location.
			Page has a side bar that shows the amount of days that the user is staying at the location with activities that were added to that day.
      This page also has a details widget that shows the detail for the current selected activity if the user chooses to get more information about it.

  Login/ Logout/ Signup: a working authentication page for users. Our stretch goals (Saving to a day, past trips, and the pay planner) are all authentication locked.


## Stretch Goals Functionality
  Past-trips: a page to show past trips per the user
	Day-planner: a page to add specific activities to a certain time per the day.
  View-itinerary : a page to view the current save activities on a certain date.



## Installation
1. Get API Keys at:
    * OpenWeatherAPI [https://openweathermap.org](https://openweathermap.org/api)
    * Trail API [https://rapidapi.com](https://rapidapi.com/trailapi/api/trailapi)
    * Places API [https://rapidapi.com](https://rapidapi.com/opentripmap/api/places1)
2. Generate a Signing Key
    Run the following commands in a shell that has openssl installed. If you’re on Windows, you can use the openssl in your WSL installation by running the program Ubuntu on Windows.
    ```sh
    openssl genrsa -out RS256.key 2048
    openssl rsa -in RS256.key -pubout -outform PEM -out RS256.key.pub
    ```
3. Fork the project
4. Clone the repo
    ```sh
    git clone https://gitlab.com/system-react/ready-set-travel
    ```
5. Create docker volumes:
    * postgres-data
    * pg-admin
6. Create a `.env` file
    Enter your API Kyes in `.env`
    ```py
    OPEN_WEATHER_API_KEY = 'ENTER YOUR API'
    SIGNING_KEY = 'ENTER YOUR API'
    TRAIL_API_KEY = 'ENTER YOUR API'
    PLACES_API_KEY = 'ENTER YOUR API'
    ```
7. Docker compose build
8. Docker compose up


## Tests
  Travel/tests
  * Jonas - test_main_page.py
  * Marvin - test_marvin.py
  * Sophia - test_sophia.py

## FrontEnd



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[React.js]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[React-url]: https://reactjs.org/
[Bootstrap.com]: https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white
[Bootstrap-url]: https://getbootstrap.com
[fastApi]: https://img.shields.io/badge/FastAPI-005571?style=for-the-badge&logo=fastapi
[fastApi-url]: https://fastapi.tiangolo.com/
[Redux.js]: https://img.shields.io/badge/redux-%23593d88.svg?style=for-the-badge&logo=redux&logoColor=white
[Redux-url]: https://redux.js.org/
[Python.org]: https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd5
[Python-url]: https://www.python.org/
[Javascript.js]: https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E
[Javascript-url]: https://www.ecma-international.org/
