### Log in

* Endpoint path: /token
* Endpoint method: POST

* Request shape (form):
  * username: string
  * password: string

* Response: Account information and a token
* Response shape (JSON):
    ```json
    {
      "account": {
        «key»: type»,
      },
      "token": string
    }
    ```


### Log out

* Endpoint path: /token
* Endpoint method: DELETE

* Headers:
  * Authorization: Bearer token

* Response: Always true
* Response shape (JSON):
    ```json
    true


### Bike Trails (TrailAPI)

* Endpoint path: /biketrails (TBD)
* Endpoint method: GET
* Query parameters:
  * lat: Latitude to search for
  * lon: Longitude to search for
  * Optional params - page/per_page/radius

* Headers:
  * Authorization: API Key

* Response: List of Bike Trails
* Response shape (JSON):
    ```json
    {
      "Trails": [
        {
          "name": str,
          "url": str,
          "lenght": int,
          "description": str,
          "difficulty": str,
          "rating": int,
          "image": url,
        }
      ]
    }
    ```


### Location - Restaurant (PLacesAPI)

* Endpoint path: /restaurants (TBD)
* Endpoint method: GET
* Query parameters:
  * locationid: id of location to search for (Search restaurant location)

* Headers:
  * Authorization: API Key

* Response: List of Restaurants
* Response shape (JSON):
    ```json
    {
      "Restaurants": [
        {
          "name": string,
          "cuisinetype": string,
          "averagerating": number,
          "pricetag": string,
          "image": url,
        }
      ]
    }
    ```


### Weather (OpenWeather)

* Endpoint path: /weather (TBD)
* Endpoint method: GET
* Query parameters:
  * lat: Latitude to search for
  * lon: Longitude to search for

* Headers:
  * Authorization: API Key

* Response: Current Weather
* Response shape (JSON):
    ```json
    {
      "Weather": [
        {
          "main": string,
          "description": string,
          "temp": number,
          "feels_like": string,
          "humidity": url,
        }
      ]
    }
    ```


### Geocode (OpenWeather)

* Endpoint path: /geocode (TBD)
* Endpoint method: GET
* Query parameters:
  * q: City name, State code (US Only), Country code
  * appid: API Key
  * limit: number of locations (optional)

* Headers:
  * Authorization: API Key

* Response: Lat and Lon for Location
* Response shape (JSON):
    ```json
    {
      "Location": [
        {
          "lat": number,
          "lon": number,
        }
      ]
    }
    ```
