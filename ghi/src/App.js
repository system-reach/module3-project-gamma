import './App.css';
import Nav from './Nav.js';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import SignupForm from './Authentication/SignupForm.js';
import LoginForm from './Authentication/LoginForm.js';
import MainPageForm from './MainPage.js';
import Landing from './components/landingContent/Landing'
import PastDayPlans from './components/past_dayPlans/PastDayPlans';
// import WeatherCard from './components/weather/Weatherdata.js';
// import TrailCard from './components/trail/Traildata.js';
// import AttractionsCard from './components/attractions/Attractionsdata';
import ContactUs from './Contact.js';


function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, '');
  return (
    <BrowserRouter basename={basename}>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/signup" element={<SignupForm />} />
          <Route path="/login" element={<LoginForm />} />
          <Route path="/contact" element={<ContactUs />} />
          <Route path="/" element={<MainPageForm />} />
          <Route path="/past" element={<PastDayPlans />} />

          {/* <Route path="/trails" element={<TrailCard />} /> */}
          {/* <Route path="/attractions" element={<AttractionsCard />} /> */}
          <Route path="/landing" element={<Landing />} />
          {/* <Route path="/weather" element={<WeatherCard />} /> */}
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
