import React from 'react'
import {Link} from 'react-router-dom'

export default function AuthModal() {
  return (
    <div className="modal" tabindex="-1">
        <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-header">
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div className="modal-body">
                    <p>Please login or create an account to save your trip!</p>
                </div>
                <div className="modal-footer">
                    <Link to="/signup" className="btn btn-primary m-3">Sign Up</Link>
                    <Link to="/" className="btn btn-primary m-3">Home</Link>
                    <Link to="/login" className="btn btn-primary m-3">Log In</Link>
                </div>
            </div>
        </div>
    </div>
  )
}
