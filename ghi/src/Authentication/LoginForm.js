import { useState } from 'react';
import { useNavigate } from 'react-router-dom'
import { useLoginMutation } from '../store/authApi';
import {useDispatch } from 'react-redux'
import { setAuth } from '../store/slices/authSlice'

function LoginForm() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [login, result] = useLoginMutation();
  const dispatch=useDispatch()
  const navigate = useNavigate()

  async function handleSubmit(e) {
    e.preventDefault();
    login({email, password}).unwrap()
    .then(({access_token})=>{
      dispatch(setAuth(access_token))
      navigate(-1, {replace:true})
    }).catch(console.log(result));
  }

  return (
      <div className="container">
        <div className="row">
          <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div className="card border-0 shadow rounded-3 my-5">
              <div className="card-body p-4 p-sm-5">
                <h5 className="card-title text-center mb-5 fw-light fs-5">Log In</h5>
                <form onSubmit={handleSubmit} id="create-account">
                  <div className="form-floating mb-3">
                    <input value={email} onChange={e => setEmail(e.target.value)} placeholder="email" required type="email" name="email" id="email" className="form-control" />
                    <label htmlFor="floatingInput">Email</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={password} onChange={e => setPassword(e.target.value)} placeholder="password" required type="password" name="password" id="password" className="form-control" />
                    <label htmlFor="floatingPassword">Password</label>
                  </div>
                  <div className="d-grid">
                    <button className="btn btn-primary btn-login text-uppercase fw-bold" type="submit">Log in</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
  );
}

export default LoginForm;
