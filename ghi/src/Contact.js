import React from "react";

function ContactUs(props) {
    return (
        <>
            <section id="team" className="pb-5">
                <div className="container">
                    <h5 className="section-title h1">System React</h5>
                    <div className="row">
                        <div className="col-xs-12 col-sm-6 col-md-4">
                            <div className="image-flip" >
                                <div className="mainflip flip-0">
                                    <div className="frontside">
                                        <div className="card">
                                            <div className="card-body text-center">
                                                <p><img className=" img-fluid" src="https://media-exp1.licdn.com/dms/image/D5603AQHJF7saTdGfUg/profile-displayphoto-shrink_100_100/0/1668473160094?e=1675296000&v=beta&t=jGkckHjzcFD0o3v2G0MpJ_4cv35qjPS5XWMq2SEyCIk" alt="card "/></p>
                                                <h4 className="card-title">Sophia Hu</h4>
                                                <h5 className="card-subtitle mb-2 text-muted">Software Developer</h5>
                                                <h6 className="card-subtitlem b-2 text-muted">Agile lead | API specialist </h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="backside">
                                        <div className="card">
                                            <div className="card-body text-center mt-4">
                                                <h4 className="card-title">Sophia Hu</h4>
                                                <p className="card-text">This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.</p>
                                                <ul className="list-inline">
                                                    <li className="list-inline-item">
                                                        <a className="social-icon text-xs-center" rel="noreferrer" target="_blank" href="https://www.linkedin.com/in/super-sophia-hu/">
                                                            <i className="fa fa-linkedin"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-6 col-md-4">
                            <div className="image-flip" >
                                <div className="mainflip flip-0">
                                    <div className="frontside">
                                        <div className="card">
                                            <div className="card-body text-center">
                                                <p><img className=" img-fluid" src="https://media-exp1.licdn.com/dms/image/C4D03AQG9QUMYL-7Arw/profile-displayphoto-shrink_800_800/0/1582743179410?e=1675296000&v=beta&t=BCDF9ZjJNbAT3yF_txK-_2pMtS68YyfSyEzGmw7Q2EY" alt="card "/></p>
                                                <h4 className="card-title">Marvin Lee</h4>
                                                <h5 className="card-subtitle mb-2 text-muted">Software Developer</h5>
                                                <h6 className="card-subtitlem b-2 text-muted">API Lead | Frontend Lead</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="backside">
                                        <div className="card">
                                            <div className="card-body text-center mt-4">
                                                <h4 className="card-title">Marvin Lee</h4>
                                                <p className="card-text">This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.</p>
                                                <ul className="list-inline">
                                                    <li className="list-inline-item">
                                                        <a className="social-icon text-xs-center"  rel="noreferrer" target="_blank" href="https://www.linkedin.com/in/marvinlee323/">
                                                            <i className="fa fa-linkedin"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-6 col-md-4">
                            <div className="image-flip" >
                                <div className="mainflip flip-0">
                                    <div className="frontside">
                                        <div className="card">
                                            <div className="card-body text-center">
                                                <p><img className=" img-fluid" src="https://media-exp1.licdn.com/dms/image/D5603AQEyXakQTlOjww/profile-displayphoto-shrink_200_200/0/1665020322588?e=1675296000&v=beta&t=9KaOZQOdtiwmveEib6FaDYd83-b1q7kOlUh16PJ7S4Q" alt="card"/></p>
                                                <h4 className="card-title">David Leung</h4>
                                                <h5 className="card-subtitle mb-2 text-muted">Software Developer</h5>
                                                <h6 className="card-subtitlem b-2 text-muted"> Backend Editor |SQL Developer </h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="backside">
                                        <div className="card">
                                            <div className="card-body text-center mt-4">
                                                <h4 className="card-title">David Leung</h4>
                                                <p className="card-text">This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.</p>
                                                <ul className="list-inline">
                                                    <li className="list-inline-item">
                                                        <a className="social-icon text-xs-center" rel="noreferrer" target="_blank" href="https://www.linkedin.com/in/thedavidleung/">
                                                            <i className="fa fa-linkedin"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-6 col-md-4">
                            <div className="image-flip" >
                                <div className="mainflip flip-0">
                                    <div className="frontside">
                                        <div className="card">
                                            <div className="card-body text-center">
                                                <p><img className=" img-fluid" src="https://media-exp1.licdn.com/dms/image/C4D03AQHwlQTE-wrlNQ/profile-displayphoto-shrink_200_200/0/1567008651898?e=1675296000&v=beta&t=yDg1-vVednAo3ZxUAOGtrrgceyoePar5z4UmoFrJADc" alt="card"/></p>
                                                <h4 className="card-title">Jonas Petit-frere</h4>
                                                <h5 className="card-subtitle mb-2 text-muted">Software Developer</h5>
                                                <h6 className="card-subtitlem b-2 text-muted">Design lead | Database editor </h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="backside">
                                        <div className="card">
                                            <div className="card-body text-center mt-4">
                                                <h4 className="card-title">Jonas Petit-frere</h4>
                                                <p className="card-text">This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.</p>
                                                <ul className="list-inline">
                                                    <li className="list-inline-item">
                                                        <a className="social-icon text-xs-center" target="_blank" rel="noreferrer" href="https://www.linkedin.com/in/jonas-pf/">
                                                            <i className="fa fa-linkedin"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}


export default ContactUs;
