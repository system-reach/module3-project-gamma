import { useState, useEffect} from 'react';
import {useNavigate} from 'react-router-dom'
import { useGetCuisineMainQuery } from './store/locationApi';
import {setStartDate, setEndDate, setCountry, setState, setCity, setCountryName} from  './store/slices/searchSlice'
import {setCuisines, setMainSummary} from './store/slices/cuisineSlice'
import {setDateArray} from './store/slices/dateSlice'
import {useDispatch, useSelector} from 'react-redux'
import moment from 'moment'
import VideoBg from './assets/VideoBg.mp4'


function MainPageForm() {
  const {startDate, endDate, state, city, countryName} = useSelector(state=>state.search)
  const [countries, setCountries] = useState([]);
  const [submitStatus, setSubmitStatus] = useState(false)


  const dispatch = useDispatch()
  const navigate = useNavigate()


  useEffect(() => {
    async function getCountries() {
      const url = 'http://localhost:8000/api/countries';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setCountries(data);
      }
    }
    getCountries();
  }, [])

  async function handleSubmit(e) {
    e.preventDefault();
    const [start, end] = [moment(startDate), moment(endDate)]
    const dateDiff = end.diff(start, 'days')
    dispatch(setDateArray(
      Array.from(Array(dateDiff+1).keys(), x=>x+1)
      ))
      setSubmitStatus(true)
    }
  const {data:mainData, isSuccess:cuisineSuccess}
    = useGetCuisineMainQuery(countryName, {skip: !submitStatus})

  useEffect(()=>{
    if (cuisineSuccess) {
      const mainSummary = Object.values(mainData.mainSummary)[0]
      dispatch(setCuisines(mainData.cuisines))
      dispatch(setMainSummary(mainSummary))
      navigate('/landing')

    }
  }, [cuisineSuccess, mainData, dispatch, navigate])

  const handleCountry=(value)=>{
    const countryCode = value.slice(-2)
    const countryName = value.slice(0,-2)

    dispatch(setCountry(countryCode))
    dispatch(setCountryName(countryName))
  }


  return (
    <div>
      <video autoPlay loop muted id='video'>
        <source src={VideoBg} type='video/mp4' />
      </video>
      <div className="row-fluid">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1 className="mainpagetext" id="mainh1">Plan Your Trip!</h1>
              <form onSubmit={handleSubmit} id="create-account">
                <div className="row">
                  <div className="col-md-6 mb-3">
                    <label className="mainpagetext" htmlFor="start_date">Start Date</label>
                    <input value={startDate} onChange={e=>dispatch(setStartDate(e.target.value))} placeholder="start_date" required type="date" name="startDateState" id="start_date" className="form-control" />
                  </div>
                  <div className="col-md-6 mb-3">
                    <label className="mainpagetext" htmlFor="end_date">End Date</label>
                    <input value={endDate} onChange={e=>dispatch(setEndDate(e.target.value))} placeholder="endDate" required type="date" name="endDateState" id="end_date" className="form-control" />
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6 mb-3">
                    <label className="mainpagetext" htmlFor="city">City</label>
                    <input value={city} onChange={e=>dispatch(setCity(e.target.value))} placeholder="City" required type="text" name="cityState" id="city" className="form-control" />
                  </div>
                  <div className="col-md-6 mb-3">
                    <label className="mainpagetext" htmlFor="state">State</label>
                    <input value={state} onChange={e=>dispatch(setState(e.target.value))} placeholder="State (Optional)" type="text" name="stateState" id="state" className="form-control" />
                  </div>
                  <div className="col-md-6 mb-3">
                    <label className="mainpagetext" htmlFor="inputState">Country</label>
                      <select onChange={e=>handleCountry(e.target.value)} required name="countryState" id="countryState" className="form-select">
                        <option value="">Select a Country</option>
                          {countries.map(country =>
                            <option key={country.name} value={country.name+country.code}>
                                {country.name}
                            </option>
                          )}
                      </select>
                  </div>
                </div>
                <button type="submit" className="btn btn-primary text-uppercase fw-bold">Submit</button>
              </form>
          </div>
        </div>
      </div>
    </div>
  );
}



export default MainPageForm;
