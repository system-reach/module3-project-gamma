import { NavLink, useNavigate } from 'react-router-dom';
import {useLogOutMutation} from './store/authApi'
import {useSelector, useDispatch} from 'react-redux'
import {setAuth} from './store/slices/authSlice'
import React from 'react'



function Nav() {
    const [logOut] = useLogOutMutation()
    const {loginToken} = useSelector((state)=>state.auth)
    const dispatch = useDispatch()
    const navigate = useNavigate()

    function logOutHandler() {
        logOut()
        dispatch(setAuth(""))
        navigate('/')
    }

    return (
        <>
            <nav className="navbar navbar-expand-lg navbar-dark">
                <div className="container-fluid">
                    <NavLink className='nav-brand' to='/'>READY, SET, TRAVEL!</NavLink>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav ms-auto">
                        <>
                            {loginToken?
                                 <li className="nav-item">
                                    <NavLink onClick={logOutHandler} className="nav-link" aria-current="page">Logout</NavLink>
                                </li>
                                :<>
                                    <li className="nav-item">
                                        <NavLink className="nav-link" aria-current="page" to="/signup">Sign Up</NavLink>
                                    </li>
                                    <li className="nav-item ">
                                        <NavLink className="nav-link" aria-current="page" to="/login">Login</NavLink>
                                    </li>
                                </>
                            }
                        </>
                            <li className="nav-item">
                                <NavLink className="nav-link" aria-current="page" to="/contact">Contact Us</NavLink>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </>
    )
}

export default Nav;
