import { useSelector } from "react-redux";

export default function AttractionsCard() {
    const {attractionsDetailArray, attractionsDetailTarget} = useSelector(state => state.detail)

    return (
        <div className="my-5 container">
            <div className="card" style={{width: 20 + "rem"}}>
                <div className="card-body">
                    <h6 className="card-title">Attractions Nearby</h6>
                        <div>
                            <p className="card-text">{ attractionsDetailArray[attractionsDetailTarget]?.name}</p>
                            <p className="card-text"> Address: { attractionsDetailArray[attractionsDetailTarget]?.address}</p>
                            <p className="card-text"> Description: { attractionsDetailArray[attractionsDetailTarget]?.description}</p>
                        </div>
                </div>
            </div>
        </div>
    )
};
