import React,{useState, useEffect} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import './Landing.css'
import Itinerary from './itinerary/Itinerary'
import InfoCards from './infocards/InfoCards'
import WeatherCard from '../weather/Weatherdata'
import TrailCard from '../trail/Traildata'
import CuisineDetail from './cuisine/CuisineDetail'
import AttractionsCard from '../attractions/Attractionsdata'
import {useGetCuisineRegionalQuery, useGetAttractionsQuery, useGetTrailQuery } from '../../store/locationApi'
import { setTrailDetailArray, setCuisineDetailArray, setAttractionsDetailArray } from '../../store/slices/detailSlice'
import { useCreateDayPlanMutation } from '../../store/authApi'

const initCollapse = {
  'attractions': false,
  'trails': false,
  'cuisines': false,
}

export default function Landing() {

  const { mainSummary, cuisines } = useSelector(state=>state.cuisine)
  const {data:cuisinesData} = useGetCuisineRegionalQuery(cuisines)

  const {startDate, endDate, city, state, country, countryName} = useSelector(state=>state.search)
  const { data:trailData } = useGetTrailQuery({city,state,country})
  const { data:attractionsData} = useGetAttractionsQuery({city,state,country})

  const {itineraryCreate} = useSelector(state=>state.itinerary)
  const {loginToken} = useSelector(state=>state.auth)
  const [collapse, setCollapse] = useState(initCollapse)
  const [createDayPlan] = useCreateDayPlanMutation()
  const dispatch = useDispatch()

  useEffect(()=>{
    if(cuisinesData) {
        const cuisineArray = cuisines.map(cuisine=>{
        console.log(cuisine)
        const [summary, images] = [cuisinesData['subSummary'][cuisine], cuisinesData['images'][cuisine]]
        const sliceIndex = summary.search(/[.](?=[ ]|\n)/)+1
        const [sentence, rest] = [summary.slice(0, sliceIndex), summary.slice(sliceIndex)]
        const name = cuisine.slice(6,).replaceAll("_"," ")
        return (
              { name: name,
                info: sentence,
                summary:rest? rest: sentence,
                images:images,}
          )
        })
      dispatch(setCuisineDetailArray(cuisineArray))
    }
  },[cuisines, cuisinesData, dispatch])

  useEffect(()=>{
    if(trailData) {
      const trailArray = trailData.trails.map((trail) =>(
        { name: trail.name[0],
          info: trail.description[0],
          description: trail.description[0],
          length: trail.length[0],
          rating: trail.rating[0],
          difficulty: trail.difficulty[0],
          thumbnail: trail.thumbnail[0],
          url: trail.url[0] }
        ))
      dispatch(setTrailDetailArray(trailArray))
    }
  }, [trailData, dispatch])

  useEffect(() => {
    if (attractionsData) {
      const attractionsArray = attractionsData.attractions.map((attraction) => ({
          name: attraction.name,
          info: attraction.description,
          description: attraction.description,
          address: attraction.address,
        })
      );
      dispatch(setAttractionsDetailArray(attractionsArray));
    }
  }, [attractionsData, dispatch])

  function createInfoCards(name) {
    return (
        <>
          <div className="px-4 py-4 my-2">
            <div className="infoCardButtonContainer no-padding">
            {/* <div className="col-lg-6 mx-auto text-center"> */}
              <button className="btn btn-large btn-primary w-100" type="button" name={name} aria-expanded={collapse[name]? false:true} onClick={e=>handleCollapse(e.target.name)} aria-controls={`${name}Card`}>
                {name.toUpperCase()}
              </button>
            </div>
          </div>
          <div className={collapse[name]? 'collapse':''} id={`${name}Card`}>
            <div className="card card-body infoCardBody">
              <InfoCards target={name}/>
            </div>
          </div>
        </>
    )
  }
  const handleCollapse=(name)=>setCollapse(
    prev=>({...prev, [name]:!prev[name]})
    )

  const planSubmitHandler=(e)=>{
    e.preventDefault()
    let trailArray = Object.entries(itineraryCreate['trails'])
    trailArray = trailArray.filter(entry=> entry[1].length !== 0)
    const trailItinerary = trailArray? Object.fromEntries(trailArray) : []

    let cuisineArray = Object.entries(itineraryCreate['cuisines'])
    cuisineArray = cuisineArray.filter(entry=>entry[1].length !== 0)
    const cuisineItinerary = cuisineArray? Object.fromEntries(cuisineArray) : []

    let attractionsArray = Object.entries(itineraryCreate["attractions"]);
    attractionsArray = attractionsArray.filter((key, value) => value !== []);
    const attractionsItinerary = Object.fromEntries(attractionsArray);

    const trailPlan = trailItinerary
    const attractionsPlan = attractionsItinerary
    const cuisinePlan = {
      mainSummary: mainSummary,
      itinerary: cuisineItinerary
    }
    const day_plan = {
      "city": city,
      "state": state,
      "country": countryName,
      "startDate": startDate,
      "endDate": endDate,
      "cuisines": cuisinePlan,
      "trails": trailPlan,
      "attractions": attractionsPlan
    }
    console.log('DAYPLAN', day_plan)
    createDayPlan(day_plan)
  }

    return (
      <>
          <div className="pageContainer">

              <div className="featureContainer itineraryContainer" >
                <Itinerary />
              </div>

              <div className="featureContainer infoCardContainer" >
                  <div className="col-lg-6 mx-auto text-center w-100">
                      <h1 className="display-6 fw-bold">Destination: {city}, {country}</h1>
                      <p className="display-10 fw-bold">Dates: {startDate} to {endDate}</p>
                      {
                        loginToken &&
                      <form onSubmit={planSubmitHandler}>
                        <button className="btn btn-primary">Save Itinerary!</button>
                      </form>
                      }
                  </div>
                  <div className='infoCard'>
                    {createInfoCards('trails')}
                  </div>
                  <div className='infoCard'>
                    {cuisinesData ? createInfoCards('cuisines')
                      :<div className="card card-body">
                        Loading Cuisines!
                      </div>
                    }
                  </div>
                  <div className='infoCard'>
                    {createInfoCards('attractions')}
                  </div>
              </div>

              <div className="featureContainer detailContainer" >
                  <div>
                    <ul className="nav nav-tabs" id="myTab" role="tablist">
                        <li className="nav-item" role="presentation">
                            <button className="nav-link active" id="weather-tab" data-bs-toggle="tab" data-bs-target="#weather-tab-pane" type="button" role="tab" aria-controls="weather-tab-pane" aria-selected="true">Weather</button>
                        </li>
                        <li className="nav-item" role="presentation">
                            <button className="nav-link" id="hiking-tab" data-bs-toggle="tab" data-bs-target="#hiking-tab-pane" type="button" role="tab" aria-controls="hiking-tab-pane" aria-selected="false">Hiking</button>
                        </li>
                        <li className="nav-item" role="presentation">
                            <button className="nav-link" id="cuisine-tab" data-bs-toggle="tab" data-bs-target="#cuisine-tab-pane" type="button" role="tab" aria-controls="cuisine-tab-pane" aria-selected="false">Cuisine</button>
                        </li>
                        <li className="nav-item" role="presentation">
                            <button className="nav-link" id="attractions-tab" data-bs-toggle="tab" data-bs-target="#attractions-tab-pane" type="button" role="tab" aria-controls="attractions-tab-pane" aria-selected="false">Attraction</button>
                        </li>
                    </ul>
                    <div className="tab-content" id="myTabContent">
                      <div className="tab-pane fade show active" id="weather-tab-pane" role="tabpanel" aria-labelledby="weather-tab" tabIndex="0">
                        <WeatherCard />
                      </div>

                      <div className="tab-pane fade" id="hiking-tab-pane" role="tabpanel" aria-labelledby="hiking-tab" tabIndex="0">
                        <TrailCard />
                      </div>

                      <div className="tab-pane fade" id="cuisine-tab-pane" role="tabpanel" aria-labelledby="cuisine-tab" tabIndex="0">
                        <CuisineDetail />
                      </div>
                      <div className="tab-pane fade" id="attractions-tab-pane" role="tabpanel" aria-labelledby="attractions-tab" tabIndex="0">
                        <AttractionsCard />
                      </div>
                    </div>
                  </div>
              </div>

          </div>
      </>
  )
}
