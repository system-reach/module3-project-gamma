import React from 'react'
import {useSelector} from 'react-redux'

export default function CuisineDetail() {
    const {cuisineDetailTarget, cuisineDetailArray} = useSelector(state => state.detail)

  return (
    <div>
        <div id="carouselExampleControls" className="carousel slide" data-ride="carousel">

            <div className="carousel-inner">
                { cuisineDetailArray[cuisineDetailTarget]?.images.map((img, index)=>
                <div className={index===0?"carousel-item active": 'carousel-item' }>
                    <img className="d-block w-100" src={img} alt={`slide ${index}`}/>
                </div>
                )}

            </div>

            <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Previous</span>
            </button>
            <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Next</span>
            </button>
        </div>

        <div className="card" >
            <div className="card-body">
                {
                    cuisineDetailArray?
                    <>
                        <h5 className="card-title">{cuisineDetailArray[cuisineDetailTarget]?.name}</h5>
                        <p className="card-text">{cuisineDetailArray[cuisineDetailTarget]?.summary}</p>
                    </>
                    : <p>Loadin Cuisines...</p>
                }

            </div>
        </div>

    </div>
  )
}
