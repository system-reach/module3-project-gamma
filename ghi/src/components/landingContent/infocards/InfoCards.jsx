import React, {useState} from 'react'
import './InfoCards.css'
import {useDispatch, useSelector} from 'react-redux'
import { setItineraryCreate } from '../../../store/slices/itinerarySlice'
import {
  setTrailDetailTarget,
  setCuisineDetailTarget,
  setAttractionsDetailTarget,
} from "../../../store/slices/detailSlice";

export default function InfoCards({target}) {
  const [selectValue, setSelectValue] = useState({})
  const {dateArray} = useSelector(state=>state.date)
  const {cuisineDetailArray, trailDetailArray, attractionsDetailArray} = useSelector(state=>state.detail)
  const dispatch = useDispatch()

  const infoCardObj = {
    detailDispatch: {
      'trails': setTrailDetailTarget,
      'cuisines': setCuisineDetailTarget,
      'attractions': setAttractionsDetailTarget,
    },
    detailObj: {
      'trails': trailDetailArray,
      'cuisines': cuisineDetailArray,
      'attractions': attractionsDetailArray,
    }
  }

  const submitHandler=(e)=>{
    const formId = e.target.id
    const [dayIndex, indexDetail] = selectValue['select'+formId]
    e.preventDefault()
    dispatch(setItineraryCreate(
        {
          target,
          dayIndex,
          activityObj:infoCardObj.detailObj[target][indexDetail]
        }
      ))
  }

  const selectHandler=(value, indexDetail, id)=>{
    const day = value.slice(-1)
    const content = value.slice(0,value.length-1)
    setSelectValue({...selectValue, [id]:[day, content, indexDetail]})
  }
  const clickHandler=(index)=>{
    console.log(index)
    dispatch(
      infoCardObj.detailDispatch[target](Number(index))
    )
  }

  return (
    <>
  <div className="card" >
    <ul className="list-group list-group-flush">
      {infoCardObj.detailObj[target]?.map(({name,info}, indexDetail )=>
          <li key={name+String(indexDetail)} className="list-group-item">
            <div className="itemContainer">
              <button className='nameContainer' onClick={e=>clickHandler(e.target.name)} name={indexDetail} >{name}</button>
              <div className='infoContainer'>{info}</div>
                <form onSubmit={submitHandler} id={'form'+name+String(indexDetail)}>
                  <div>
                    <select
                    onChange={e=>selectHandler(e.target.value, indexDetail, e.target.id)}
                    id={'selectform'+name+String(indexDetail)}
                    className='form-select' required>
                      <option value="">Choose a Day</option>
                        {dateArray.map((date, indexDate)=>
                          <option key={`select-${indexDate}`} value={name+indexDate}>
                            {`Day ${date}`}
                          </option>
                          )}
                    </select>
                  </div>
                  <button className="btn btn-primary mx-3">Save to day!</button>
                </form>
            </div>
          </li>
        )
      }

    </ul>
  </div>


    </>
  )
}
