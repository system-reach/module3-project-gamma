import React, {useState} from 'react'
import './Itinerary.css'
import {useSelector, useDispatch} from 'react-redux'
import { deleteItineraryCreate } from '../../../store/slices/itinerarySlice'



export default function Itinerary() {
  const {dateArray} = useSelector((state)=>state.date)
  const {itineraryCreate} = useSelector(state=>state.itinerary)

  const initCollapse = Object.fromEntries(dateArray.map(date=>([String(date), false])))
  const [collapse, setCollapse] = useState(initCollapse)
  const dispatch = useDispatch()

  const handleCollapse = (name)=>{
    setCollapse(prev=>{return {...prev, [name]:!prev[name]} })
  }
  const deleteHandler = (category, dayIndex, name) => {
    dispatch(deleteItineraryCreate({category, dayIndex, name}))

  }
  const datePlanner = Array.from(dateArray, (_,index) => {
    return (
      <div key={index} className="dayCard">
        <div className="col-lg-12 mx-8 text-center">
          <button className="btn btn-primary" name={index} type="button"  aria-expanded={collapse[index]? false:true} onClick={e=>handleCollapse(e.target.name)} aria-controls={`dayCard${index}`}>
            Day {index+1}
          </button>
        </div>
        <div className={collapse[index]? 'collapse cardContainer w-100':'cardContainer w-100'} id={`dayCard${index}`}>
          <div className="card itinerary-card-body">
            <ul className="list-group">
              {Object.keys(itineraryCreate).map(category=>{
                const itinerary = itineraryCreate[category]
                return Object.keys(itinerary).map(day=>
                  day===String(index) && itinerary[day].map(activity=>
                      <li>
                        {category}: {activity.name}
                        <button
                          onClick={e=>deleteHandler(e.target.value, e.target.name, e.target.id.slice(9))}
                          value={category} name={day} id={`itinerary${activity.name}`}
                          >Delete
                        </button>
                      </li>
                    )
                  )
                })
              }
            </ul>
          </div>
        </div>
      </div>
    )
  })

  return datePlanner
}
