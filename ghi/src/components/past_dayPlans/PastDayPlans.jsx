import React, {useEffect, useState} from 'react'
import { useGetAllDayPlansQuery, useGetOneDayPlanQuery } from '../../store/authApi'
// import { setTrailPastArray, setTrailPastTarget } from '../../store/slices/pastDetailSlice'


export default function PastDayPlans() {
    const [dayplans, setDayPlans] = useState([])
    const [queryTarget, setQueryTarget] = useState(0)
    const {data:getAllData, isSuccess:getAllSuccess} = useGetAllDayPlansQuery()
    const {data:getOneData, isSuccess:getOneSuccess} = useGetOneDayPlanQuery(queryTarget)


    useEffect(()=>{
      if (getOneSuccess) {
        console.log(getOneData)

      }
    }, [getOneSuccess, getOneData])
    useEffect(()=>{
      if (getAllSuccess) {
        console.log(getAllData)
        setDayPlans(getAllData.dayPlans)
      }
    }, [getAllSuccess, getAllData, setDayPlans])
    const clickHandler=(id)=>{
      setQueryTarget(id)
    }
  return (
    <div>
      {dayplans && dayplans.map((day, index)=>
      <div>

      <button key={day.id+index} onClick={e=>clickHandler(e.target.value)} value={day.id}>
        {JSON.stringify(day)}
      </button>
      </div>
      )}

    </div>
  )
}
