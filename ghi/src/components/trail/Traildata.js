import { useSelector } from "react-redux";

export default function TrailCard() {
    const {trailDetailArray, trailDetailTarget} = useSelector(state => state.detail)

    return (
        <div className="my-5 container">
            <div className="card" style={{width: 20 + "rem"}}>
                <div className="card-body">
                    <h6 className="card-title">Trails Nearby</h6>

                        <div>
                            <img className="card-img-top" src={ trailDetailArray[trailDetailTarget]?.thumbnail} alt="No image available"></img>
                            <p className="card-text">{ trailDetailArray[trailDetailTarget]?.name}</p>
                            <p className="card-text">{ trailDetailArray[trailDetailTarget]?.description}</p>
                            <p className="card-text"> Length: { trailDetailArray[trailDetailTarget]?.length} mi</p>
                            <p className="card-text"> rating: { trailDetailArray[trailDetailTarget]?.rating}</p>
                            <p className="card-text"> Difficulty: { trailDetailArray[trailDetailTarget]?.difficulty}</p>
                            {/* <p className="card-text"> img: { trailDetailArray[trailDetailTarget]?.thumbnail}</p> */}
                            <p className="card-text"> website: { trailDetailArray[trailDetailTarget]?.url}</p>

                        </div>

                </div>
            </div>
        </div>
    )
};
