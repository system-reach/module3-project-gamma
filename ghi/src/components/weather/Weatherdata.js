import { useGetWeatherQuery } from '../../store/locationApi';
import { useSelector } from 'react-redux';



export default function WeatherCard() {

  const { city } = useSelector((state) => state.search);
  const { country } = useSelector((state) => state.search);
  const { state } = useSelector((state) => state.search);
  const { data } = useGetWeatherQuery({city,state,country})

  return (
    <div className="my-5 container">
      <div className="card" style={{width: 18 + "rem"}}>
        <div className="card-header">Destination Weather</div>
        <div className="card-body">
          <h5 className="card-title">{city}</h5>
          {data?
            <div>
              <p></p>
              <p className="card-text">Current Temperature: {data.temp} °F</p>
              <p className="card-text">Current Weather: {data.main}</p>
            </div>:<></>
          }
        </div>
      </div>
    </div>
  )
};
