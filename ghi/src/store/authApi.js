import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'


export const authApi = createApi({
  reducerPath: 'authentication',
  // tagTypes: ['Token'],
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_SAMPLE_SERVICE_API_HOST,
      prepareHeaders: (headers, { getState }) => {
        // const selector = authApi.endpoints.getToken.select();
        // const { data: tokenData } = selector(getState());
        // if (tokenData && tokenData.access_token) {
        //   headers.set('Authorization', `Bearer ${tokenData.access_token}`);
        // }
        const token = getState().auth.loginToken
        if (token) {
          headers.set('Authorization', `Bearer ${token}`)
        }
        console.log("HEADERS", headers)
      return headers;
    }
  }),
  tagTypes: ['Account', 'past_dayPlans', "Token"],
  endpoints: builder => ({
    login: builder.mutation({
      query: info => {
        let formData = null;
        if (info instanceof HTMLElement) {
          formData = new FormData(info);
        } else {
          formData = new FormData();
          formData.append('username', info.email);
          formData.append('password', info.password);
        }
        return {
          url: '/token',
          method: 'post',
          body: formData,
          credentials: 'include',
        };
      },
      invalidatesTags: result => {
        return (result && ['Account']) || [];
      },
    }),
    getToken: builder.query({
      query: () => ({
        url: '/token',

        credentials: 'include',
      }),
      providesTags: ['Token'],
    }),
    signup: builder.mutation({
      query: data => ({
        url: '/api/accounts',
        body: data,
        method: 'post'
      }),
      providesTags: ['Account'],
      invalidatesTags: result => {
        return (result && ['Token']) || [];
      },
    }),
    getAccounts: builder.query({
      query: () => '/api/accounts',
    }),
    logOut: builder.mutation({
      query: () => ({
        url: '/token',
        method: 'delete',
        credentials: 'include',
      }),
      invalidatesTags: ['Account', 'Token', 'past_dayPlans'],
    }),
    getAllDayPlans: builder.query({
      query: () => ({
        url: '/dayplan',
        credientials: 'include'
      }),
      providesTags: ['past_dayPlans']
    }),
    getOneDayPlan: builder.query({
      query: dayPlan_id => ({
        url: `/dayplan/${dayPlan_id}`,
        credientials: 'include'
      }),
      providesTags: ['past_dayPlans']
    }),
    createDayPlan: builder.mutation({
      query: data => ({
        url: '/dayplan',
        method: 'post',
        body: data,
        credientials: 'include'
      }),
      invalidatesTags: ['past_dayPlans']
    })
  }),
});

export const { useGetTokenQuery, useLoginMutation, useLogOutMutation, useSignupMutation, useGetAccountsQuery, useGetAllDayPlansQuery, useGetOneDayPlanQuery, useCreateDayPlanMutation} = authApi;
