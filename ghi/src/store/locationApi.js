import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'


export const locationApi = createApi({
  reducerPath: "location",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_SAMPLE_SERVICE_API_HOST,
  }),
  endpoints: (builder) => ({
    getLocation: builder.query({
      query: (data) => ({
        url: `api/locations?city=${data.city}&country=${data.country}&state=${data.state}`,
      }),
    }),
    getWeather: builder.query({
      query: (data) => ({
        url: `api/weather?city=${data.city}&country=${data.country}&state=${data.state}`,
      }),
    }),
    getTrail: builder.query({
      query: (data) => ({
        url: `api/trails?city=${data.city}&country=${data.country}&state=${data.state}`,
      }),
    }),
    getAttractions: builder.query({
      query: (data) => ({
        url: `api/attractions?city=${data.city}&country=${data.country}&state=${data.state}`,
      }),
    }),
    getCuisineMain: builder.query({
      query: (data) => ({
        url: "api/cuisines/main",
        params: { country: data },
      }),
    }),
    getCuisineRegional: builder.query({
      query: (data) => {
        const params = new URLSearchParams(data.map(cuisine=>
          ['cuisines', cuisine]
        ))
        return ({
            url: '/api/cuisines/sub/',
            params: params
          })
      }
    }),
    createTrip: builder.mutation({
      query: (data) => ({
        url: "trips",
        body: data ? data : [],
        method: "post",
      }),
    }),
  }),
});

export const { useGetLocationQuery, useGetWeatherQuery, useCreateTripMutation, useGetCuisineMainQuery, useGetCuisineRegionalQuery, useGetTrailQuery, useGetAttractionsQuery } = locationApi;
