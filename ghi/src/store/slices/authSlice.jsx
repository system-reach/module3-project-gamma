import {createSlice} from '@reduxjs/toolkit'

const initialState = {loginToken:""}
const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        setAuth: (state, {payload})=>{state.loginToken=payload}
    }
})

export const {setAuth} = authSlice.actions
export default authSlice.reducer
