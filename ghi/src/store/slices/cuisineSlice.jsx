import {createSlice} from '@reduxjs/toolkit'

const initialState={
    mainSummary: "",
    cuisines: [],
}

const cuisineSlice = createSlice({
    name:'cuisine',
    initialState,
    reducers: {
        setMainSummary: (state, {payload}) => {state.mainSummary=payload},
        setCuisines: (state, {payload}) => {state.cuisines=payload},
    }
})

export const {setMainSummary, setCuisines } = cuisineSlice.actions
export default cuisineSlice.reducer
