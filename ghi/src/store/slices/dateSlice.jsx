import {createSlice} from '@reduxjs/toolkit'

const initialState = {
    dateArray:[],
}

const dateSlice = createSlice({
    name: 'date',
    initialState,
    reducers: {

         setDateArray: (state, {payload}) => {
            state.dateArray = payload
         }
    }

})

export const { setDateArray } = dateSlice.actions
export default dateSlice.reducer
