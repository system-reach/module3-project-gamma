import {createSlice} from '@reduxjs/toolkit'
const initialState= {
  trailDetailTarget: 0,
  cuisineDetailTarget: 0,
  attractionsDetailTarget: 0,

  trailDetailArray: [],
  cuisineDetailArray: [],
  attractionsDetailArray: [],
};

const detailSlice= createSlice({
    name: 'detail',
    initialState,
    reducers: {
        setTrailDetailTarget: (state, {payload}) => {state.trailDetailTarget=payload},
        setCuisineDetailTarget: (state, {payload}) => {state.cuisineDetailTarget=payload},
        setAttractionsDetailTarget: (state, {payload}) => {state.attractionsDetailTarget=payload},
        setCuisineDetailArray: (state, {payload}) => {state.cuisineDetailArray=payload},
        setTrailDetailArray: (state, {payload}) => {state.trailDetailArray=payload},
        setAttractionsDetailArray: (state, {payload}) => {state.attractionsDetailArray = payload;},
    }
})

export const {setTrailDetailTarget, setCuisineDetailTarget, setAttractionsDetailTarget, setTrailDetailArray, setCuisineDetailArray, setAttractionsDetailArray} = detailSlice.actions
export default detailSlice.reducer
