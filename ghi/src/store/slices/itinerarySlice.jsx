import {createSlice} from '@reduxjs/toolkit'

const initialState = {
    itineraryCreate:{
        'trails': {
            "0":[]
        },
        'cuisines': {
            "0":[]
        },
        'attractions': {
            "0":[]
        }
    }
}

const itinerarySlice = createSlice({
    name:'itinerary',
    initialState,
    reducers: {
        setItineraryCreate: (state, {payload}) => {
            const {target, dayIndex, activityObj} = payload
            const create = state.itineraryCreate[target]

            state.itineraryCreate[target] =
            create.hasOwnProperty(dayIndex)?
                create[dayIndex].some(obj=>obj.info===activityObj.info)?
                    {...create}
                    :{...create, [dayIndex]:[...create[dayIndex], activityObj]}
                :{...create, [dayIndex]:[activityObj]}
        },
        deleteItineraryCreate: (state, {payload}) => {
            const {category, dayIndex, name} = payload
            const itinerary = state.itineraryCreate[category][dayIndex]
            const newItinerary = itinerary.filter(activity=>activity.name!==name)
            state.itineraryCreate[category][dayIndex] = newItinerary
        }

    }
})


export const {setItineraryDisplay, setItineraryCreate, deleteItineraryDisplay, deleteItineraryCreate} = itinerarySlice.actions
export default itinerarySlice.reducer
