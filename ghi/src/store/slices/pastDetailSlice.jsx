import {createSlice} from '@reduxjs/toolkit'

const initialState = {
    trailPastTarget: 0,
    cuisinePastTarget: 0,

    trailPastArray:[],
    cuisinePastArray:[],
}

const pastDetailSlice = createSlice({
    name: 'pastDetail',
    initialState,
        reducers: {
        setTrailPastTarget: (state, {payload}) => {state.trailPastTarget=payload},
        setCuisinePastTarget: (state, {payload}) => {state.cuisinePastTarget=payload},
        setCuisinePastArray: (state, {payload}) => {state.cuisinePastArray=payload},
        setTrailPastArray: (state, {payload}) => {state.trailPastArray=payload},
    }
})

export const {setTrailPastArray, setTrailPastTarget, setCuisinePastArray, setCuisinePastTarget} = pastDetailSlice.actions
export default pastDetailSlice.reducer
