import {createSlice} from '@reduxjs/toolkit'

const initialState = {
    startDate: "",
    endDate:"",
    country:"",
    state:"",
    city:"",
    countryName:"",
}

const searchSlice = createSlice({
    name:'search',
    initialState,
    reducers: {
        setStartDate: (state,action) => { state.startDate = action.payload },
        setEndDate: (state,action) => { state.endDate = action.payload },
        setCountry: (state,action) => { state.country = action.payload },
        setState: (state,action) => { state.state = action.payload },
        setCity: (state,action) => { state.city = action.payload },
        setCountryName: (state, action) =>{state.countryName = action.payload},
    }
})

export const {setStartDate, setEndDate, setCountry, setState, setCity, setCountryName} = searchSlice.actions

export default searchSlice.reducer
