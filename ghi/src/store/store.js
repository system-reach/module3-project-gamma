import { configureStore } from '@reduxjs/toolkit'
import { setupListeners } from '@reduxjs/toolkit/query'
import { authApi } from './authApi';
import { locationApi } from './locationApi';


import searchSliceReducer from './slices/searchSlice'
import authSliceReducer from './slices/authSlice'
import dateSliceReducer from './slices/dateSlice'
import cuisineSliceReducer from './slices/cuisineSlice'
import itinerarySliceReducer from './slices/itinerarySlice'
import detailSliceReducer from './slices/detailSlice';
import pastDetailSliceReducer from './slices/pastDetailSlice'

export const store = configureStore({
  reducer: {
    [authApi.reducerPath]: authApi.reducer,
    [locationApi.reducerPath]: locationApi.reducer,
    search: searchSliceReducer,
    date: dateSliceReducer,
    auth: authSliceReducer,
    cuisine: cuisineSliceReducer,
    itinerary: itinerarySliceReducer,
    detail: detailSliceReducer,
    pastDetail: pastDetailSliceReducer,

  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware().concat(authApi.middleware).concat(locationApi.middleware),
});

setupListeners(store.dispatch);
