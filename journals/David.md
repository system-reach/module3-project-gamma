Our team finished wireframing the app on excalidraw, listing out possible features that our app will have.

11/17
Our team created a roadmap and due dates for when we expect the backend to be finished, when we expect the frontend to be finished and some time to add finishing touches/ edits.

---

Took time to catch up on DSA explorations, FAST apis and sql

11/28 -11/29
worked on create_attractions table which depended on day_plan(id) -kept getting error "psycopg.errors.UndefinedTable:relation "day_plan" does not exist"
resolved by - working on create_day_plan which depend on accounts - Marvin already created the accounts table
finished and merged with main - Jonas' create_day_plan was almost the same as mine, just with the addition of ON DELETE CASCADE - accepted changes

11/30
Created an endpoint for creating attractions data
tested it - there was an issue with time : datetime - changed it to time instead
the create endpoint is finally working

12/1 - 12/2
finished the rest of the end points for location event - get all location event and delete location event
create response models for all location event endpoints
tested all attractions data endpoints

12/3
changed names from location event to attraction data to accurately reflect type of data - checked endpoints - still working after the change

12/4
created attractions.py in routers and queries to intake the latitude and longitude coordinates from the weather API
having issues correctly implementing it
Realized that the current request that requires the latitude and longitude only returns "xid" and "name" of the attraction
Modified code to use the XID for a different request to render addtional information about the attraction (house number, road, city, state, postal code, country, wikipedia extracts)

12/5
sought out assistance from team as requested kept rendering 500 internal server error
figured out the main reason, data was not returning properly was because in some cases, those fields do not exist
created if, else statements as try-except blocks to prevent the 500 error
sucessfully tested out the endpoints with weatherAPI's lat and long info by inputing city, state, country - returning name of attraction and description

12/6 - 12/7
created an attractiondata.js, added mutation to locationApi.js
data wasn't populating on the http://localhost:3000/attractions
managed to get data to populate in the infocards
had to merge with sophia's branch as changes were made to the inputed value of the country (country code to country code and name)

12/8-12/9
had several unsuccessful merge attempts
trail folder was moved by Sophia and integrated into the components folder, my attractions components was never pulled, needed to try and integrate it in
moved attractions into the components folder and tried modifying it to resemble the modifications made to trails
data is not populating on the infocards
getting cors issues

12/10
was getting cors issues and trails was not populating after several merging incorrectly, pull from jonas' branch, re-added back in attractions.py and attractions_data.py for queries and routers, added and correctly integrated attractions in the components folder, added route to attractions for attractions card, added getAttraction mutation, created useEffect for attractions, created an info card for attractions, created an attractions card -no cors errors and confirmed all infocards and detailcards are populating

12/11 - 12/12
Our team spoke with Riley on Friday, he advised us to change our requests from POST to GET as it was not RESTful
Marvin fixed the backend and frontend for the weather's and trail's requests - was advised to merge with Sophia's branch and fix attractions' requests
Merged with Sophia's branch,fixed the frontend and backend request for attractions, Updated the sql table for create attractions to save to day plan itinerary
created test for getting attractions
