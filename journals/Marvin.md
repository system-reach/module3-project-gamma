## December 12, 2022

Today, I worked on:

* Readme and cleaning up code


## December 9, 2022

Today, I worked on:

* Working on changing post requests for weather and location to get requests


## December 8, 2022

Today, I worked on:

* CSS


## December 7, 2022

Today, I worked on:

* CSS
* Readme


## December 6, 2022

Today, I worked on:

* Weather card CSS
* Unit tests for countries list and account list passing


## December 4, 2022

Today, I worked on:

* Get account token functional network displays token after signing in


## December 2, 2022

Today, I worked on:

* Weather query functional with inputted values from main page


## November 30, 2022

Today, I worked on:

* Displaying weather information for queried location


## November 29, 2022

Today, I worked on:

* Working on populating countries on destination form (Create table with list of countries or call an api?)


## November 28, 2022

Today, I worked on:

* Started on Main Page Form and testing location and weather api using user inputs from the form


## November 27, 2022

Today, I worked on:

* Weather API from Openweather to get weather for the location queried,
* State param optional for foreign countries


## November 26, 2022

Today, I worked on:

* Geocode API from Openweather to get lat and lon for the location queried


## November 23, 2022

Today, I worked on:

* Login Form, Signout


## November 22, 2022

Today, I worked on:

* Signup Form Functional, revisit layout and design in the future


## November 21, 2022

Today, I worked on:

* Preventing duplicate account creation and getting account tokens
* Completed GET all accounts api


## November 18, 2022

Today, I worked on:

* Create account finished


## November 17, 2022

Today, I worked on:

* Setting up JWT Authentication, Login and Logout finished


## November 16, 2022

Today, I worked on:

* Setting up PostgreSQL database and pgAdmin
