## 11/21 and before
* Tried using wikipedia API
        not working as needed
* Switch method to scraping with BeautifulSoup:
        successful
* Allowed Cors

## 11/30
* prelim redux toolkit, rtkQuery, main search page

## 12/1
* prelim Itinerary.jsx
* prelim infoCards.jsx
* added dateSlice
* added searchSlice and searchApi


## 12/2
* added accountSlice
* added tripsApi
* prelim PastTrips.jsx
* merged with jonas, marvin repo to form a mega repo
* weatherdata now up
* auth token endpoints up
* oops accidentally deleted my repo

## 12/3
* fixed nav
* fixed login

## 12/5
* wikiScrape up
* api/cuisines endpoint up

## 12/6
* wikiScrape separate into two different calls to improve fetch time
* updated Landing
* updated infoCard
* added detail component
* updated itineray
* added itinerarySlice
* updated dateSlice
* added cuisineMainQuery and cuisineRegionalMutation
* infoCards up

## 12/7
* itinerary up
* landing css, itinerary css up
* added detailSlice

## 12/8
* trailDetail up
* cuisineDetail up
* landing up
* merged from marvin&jonas, solved conflicts
* upgraded itinerary and infocards
* prelim past dayplans queries, migrations, routes

## 12/9
* create dayplans queries, migrations, routes up
* updated trails & cuisine schema
* adjusted trails & cuisine repo.create() to interact w/ create dayplans route
* split itinerarySlice state from {} => itineraryDisplay & itineraryDetailTarget
* updated itinerarySlice up
* adjusted infoCards interaction w/ itinerarySlice

## 12/10
* dayplan queries, routes, migrations up

## 12/11
* GetDayplan queries, routes up
* made test for create_cuisine

## 12/12
* fixed empty itinerary insertion raising error

## 12/13
* combined itineraryDisplay and itineraryCreate => itineraryCreate
* fixed deletion error
* fixed pydantic wrapper for attractions
* changed regionalCuisineMutation => regionalCuisineQuery
