from bs4 import BeautifulSoup
import requests

baseURL = "https://en.wikipedia.org"
apiURL = "https://en.wikipedia.org/w/api.php?"


def get_pageId(target, type):
    search = f"{target} {type}"
    params = {
        "action": "query",
        "list": "search",
        "srsearch": search,
        "format": "json",
    }
    r = requests.get(apiURL, params=params)

    return r.json()["query"]["search"][0]["pageid"]


def get_cuisineLinks(pageid):
    params = {"curid": pageid}
    htmlRes = requests.get(baseURL, params=params)
    soup = BeautifulSoup(htmlRes.text, "html.parser")
    sidebar = soup.find("table", "sidebar")
    sideLinks = sidebar.find_all("a", title=True)
    cuisines = []
    for link in sideLinks:
        if "cuisine" in link["href"] and "Template" not in link["href"]:
            cuisines.append(link["href"])
    return cuisines


def get_summary(page_title=None, page_id=None):
    target = page_title[6:] if page_title else page_id
    params = {
        "action": "query",
        "format": "json",
        "titles" if page_title else "pageids": target,
        "prop": "extracts",
        "exintro": "",
        "explaintext": "",
    }
    r = requests.get(apiURL, params=params)
    res = r.json()["query"]["pages"]
    key = list(res.keys())[0]
    res = res[key]["extract"]
    return {page_title if page_title else page_id: res}


def get_cuisine_imgLinks(cuisine):
    regionalURL = baseURL + cuisine
    regionalRes = requests.get(regionalURL)
    regSoup = BeautifulSoup(regionalRes.text, "html.parser")
    img = regSoup.find_all("a", "image")
    imgLinks = []
    for item in img:
        tmp = item["href"]
        if ".svg" not in tmp:
            imgLinks.append(item["href"])

    return {cuisine: imgLinks}


def get_imgSrc(imgLinks):
    imgSrc = []
    for link in imgLinks:
        res = requests.get(baseURL + link)
        linkSoup = BeautifulSoup(res.text, "html.parser")
        img = linkSoup.find("div", class_="fullImageLink").find("img")
        # linkDesc = linkSoup.find('td', class_="description")
        imgSrc.append("https:" + img["src"])

    return imgSrc
