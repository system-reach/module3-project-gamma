from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from authenticator import authenticator
from routers import (
    accounts,
    attractions,
    attractions_data,
    trip_data,
    locations,
    trail_data,
    day_plan,
    countries,
    trails,
    cuisines,
)

import os

app = FastAPI(debug=True)
origins = [
    "http://www.localhost:3000",
    "http://localhost:3000",
    "localhost:3000",
    "https://www.localhost:3000/",
    "http://www.localhost:8000",
    "http://localhost:8000",
    "localhost:8000",
    "https://www.localhost:8000/",
    os.environ.get("CORS_HOST", None),
    "https://system-react.gitlab.io/ready-set-travel/",
    "https://system-react.gitlab.io/",
    "https://system-react.gitlab.io",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    expose_headers=["*"],
)


app.include_router(authenticator.router)
app.include_router(accounts.router)
app.include_router(trip_data.router)
app.include_router(attractions.router)
app.include_router(attractions_data.router)
app.include_router(trail_data.router)
app.include_router(day_plan.router)
app.include_router(locations.router)
app.include_router(trails.router)
app.include_router(countries.router)
app.include_router(cuisines.router)
