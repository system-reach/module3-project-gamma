steps = [
    [
        # Create the table
        """
        CREATE TABLE accounts (
            id SERIAL PRIMARY KEY NOT NULL,
            email VARCHAR(50) NOT NULL UNIQUE,
            password VARCHAR(1000) NOT NULL,
            full_name VARCHAR(50) NOT NULL
        );
        """,
        # Drop the table
        """
        DROP TABLE accounts;
        """,
    ]
]
print(__name__)
