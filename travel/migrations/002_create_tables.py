steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE trip_data (
            id SERIAL PRIMARY KEY NOT NULL,
            owner_id INT NOT NULL REFERENCES accounts(id) ON DELETE CASCADE,
            start_date DATE NOT NULL,
            end_date DATE NOT NULL,
            country VARCHAR(50) NOT NULL,
            city VARCHAR(50) NOT NULL,
            state VARCHAR(50)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE trip_data;
        """,
    ],
]
print(__name__)
