steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE day_plan (
            id      SERIAL PRIMARY KEY NOT NULL,
            owner_id  INT NOT NULL REFERENCES accounts(id) ON DELETE CASCADE,
            city TEXT NOT NULL,
            state TEXT,
            country TEXT NOT NULL,
            startDate date NOT NULL,
            endDate date NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE day_plan ;
        """,
    ]
]
print(__name__)
