steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE trail_data (
            id SERIAL PRIMARY KEY NOT NULL,
            day_plan_id INT NOT NULL REFERENCES day_plan(id) ON DELETE CASCADE,
            day_index INT NOT NULL,
            name VARCHAR(60) NOT NULL,
            info TEXT,
            description TEXT,
            length VARCHAR(20),
            difficulty VARCHAR(50) ,
            rating SMALLINT ,
            thumbnail TEXT,
            url  TEXT,
            time TIME
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE trail_data;
        """,
    ]
]
print(__name__)
