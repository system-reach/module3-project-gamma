steps = [
    [
        ## Create the table
        """
        CREATE TABLE main_cuisine (
            id SERIAL PRIMARY KEY NOT NULL,
            day_plan_id INT NOT NULL REFERENCES day_plan(id) ON DELETE CASCADE,
            country TEXT NOT NULL,
            summary TEXT NOT NULL
        );
        """,
        ## Drop the table
        """
        DROP TABLE accounts;
        """,
    ]
]
print(__name__)
