steps = [
    [
        ## Create the table
        """
        CREATE TABLE sub_cuisine (
            id SERIAL PRIMARY KEY NOT NULL,
            main_cuisine_id INT NOT NULL REFERENCES main_cuisine(id) ON DELETE CASCADE,
            day_index INT NOT NULL,
            name TEXT NOT NULL,
            info TEXT NOT NULL,
            summary TEXT
        );
        """,
        ## Drop the table
        """
        DROP TABLE accounts;
        """,
    ]
]
print(__name__)
