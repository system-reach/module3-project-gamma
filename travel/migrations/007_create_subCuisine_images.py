steps = [
    [
        ## Create the table
        """
        CREATE TABLE subCuisine_image (
            id SERIAL PRIMARY KEY NOT NULL,
            sub_cuisine_id INT NOT NULL REFERENCES sub_cuisine(id) ON DELETE CASCADE,
            image TEXT NOT NULL
        );
        """,
        ## Drop the table
        """
        DROP TABLE accounts;
        """,
    ]
]
print(__name__)
