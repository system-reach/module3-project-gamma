steps = [
    [
        ## Create the table
        """
        CREATE TABLE attractions_data (
            id SERIAL PRIMARY KEY NOT NULL,
            day_plan_id INT NOT NULL REFERENCES day_plan(id) ON DELETE CASCADE,
            day_index INT NOT NULL,
            name VARCHAR(1000) NOT NULL,
            info TEXT,
            address TEXT,
            description TEXT,
            time TIME NOT NULL
        );
        """,
        ## Drop the table
        """
        DROP TABLE attractions_data;
        """,
    ]
]
