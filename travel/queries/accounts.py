from pydantic import BaseModel
from queries.pool import pool


class DuplicateAccountError(ValueError):
    pass


class Account(BaseModel):
    id: int
    email: str
    full_name: str


class AccountIn(BaseModel):
    email: str
    password: str
    full_name: str


class AccountOut(BaseModel):
    id: int
    email: str
    hashed_password: str
    full_name: str


class AccountList(BaseModel):
    accounts: list[Account]


class AccountQueries:
    def get(self, email: str) -> AccountOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT id
                        , email
                        , password
                        , full_name
                    FROM accounts
                    WHERE email = %s;
                    """,
                    [email],
                )
                record = result.fetchone()
                if record is None:
                    return None
                return AccountOut(
                    id=record[0],
                    email=record[1],
                    hashed_password=record[2],
                    full_name=record[3],
                )

    def get_all(self):
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id
                        , email
                        , password
                        , full_name
                    FROM accounts
                    """
                )
                results = []
                rows = db.fetchall()
                for row in rows:
                    user = {}
                    for i, column in enumerate(db.description):
                        user[column.name] = row[i]
                    results.append(user)
                return results

    def create(self, account: AccountIn, hashed_password: str) -> AccountOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                try:
                    result = db.execute(
                        """
                        INSERT INTO accounts (email, password, full_name)
                        VALUES (%s, %s, %s)
                        RETURNING id;
                        """,
                        [account.email, hashed_password, account.full_name],
                    )
                    id = result.fetchone()[0]
                    return AccountOut(
                        id=id,
                        email=account.email,
                        hashed_password=hashed_password,
                        full_name=account.full_name,
                    )
                except Exception:
                    raise DuplicateAccountError
