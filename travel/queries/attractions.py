from pydantic import BaseModel
from typing import Optional, List
import requests
import os
import json

PLACES_API_KEY = os.environ.get("PLACES_API_KEY")


class AttractionIn(BaseModel):
    lang: str
    radius: int
    lon: float
    lat: float
    kinds: str


class AttractionInfoIn(BaseModel):
    id: int


class AttractionOut(BaseModel):
    attractions: List


class XidsOut(BaseModel):
    xids: List
