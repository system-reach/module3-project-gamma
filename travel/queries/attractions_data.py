from pydantic import BaseModel
from typing import List, Optional, Union, Dict
from datetime import time
from queries.pool import pool


class Error(BaseModel):
    message: str


class AttractionIn(BaseModel):
    day_plan_id: int
    day_index: int
    name: str
    info: str
    address: str
    description: Optional[str]
    time: Optional[time]


class AttractionOut(BaseModel):
    id: int
    day_plan_id: int
    day_index: int
    name: str
    info: str
    address: str
    description: Optional[str]
    time: Optional[time]


class AttractionsRepository:
    def get_all(self, dayPlan_id: int) -> Union[Error, List[AttractionOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                            attractions_data.id,
                            attractions_data.day_plan_id,
                            attractions_data.day_index,
                            attractions_data.name,
                            attractions_data.info,
                            attractions_data.address,
                            attractions_data.description,
                            attractions_data.time,
                        FROM attractions_data
                        WHERE attractions_data.day_index = %s
                        Order by day_index;
                        """,
                        [dayPlan_id],
                    )
                    rows = result.fetchall()
                    return [
                        {
                            "id": row[0],
                            "day_plan_id": row[1],
                            "day_index": row[2],
                            "name": row[3],
                            "info": row[4],
                            "address": row[5],
                            "description": row[6],
                            "time": row[7],
                        }
                        for row in rows
                    ]

        except Exception as e:
            print(e)
            return {"message": "The list doesnt Exist"}

    def create(self, attractions: AttractionIn) -> AttractionOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO attractions_data (
                            day_plan_id,
                            day_index,
                            name,
                            info,
                            address,
                            description,
                            time
                        )
                        VALUES
                            (%s, %s, %s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            attractions.day_plan_id,
                            attractions.day_index,
                            attractions.name,
                            attractions.info,
                            attractions.address,
                            attractions.description,
                            attractions.time,
                        ],
                    )
                    id = result.fetchone()[0]
                    old_data = attractions.dict()
                return AttractionOut(id=id, **old_data)
        except Exception as e:
            print(e)
            return {"message": "Could not set that attraction"}

    def delete(self, attractions_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        DELETE FROM attractions_data
                        WHERE id = %s
                        """,
                        [attractions_id],
                    )
                    return True
        except Exception as e:
            return False
