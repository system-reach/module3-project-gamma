import requests
import json


class CountryQueries:
    def get_countries(self):
        countries = []
        url = "https://restcountries.com/v2/all?fields=name,alpha2Code"
        response = requests.get(url)
        content = json.loads(response.content)
        for country in content:
            countries.append(
                {"name": country["name"], "code": country["alpha2Code"]}
            )
        return countries
