from common.wikiScrape import (
    get_pageId,
    get_cuisineLinks,
    get_summary,
    get_cuisine_imgLinks,
    get_imgSrc,
)

from pydantic import BaseModel
from typing import Optional, Union, List, Dict
from datetime import date
from collections import ChainMap
from queries.pool import pool


class Error(BaseModel):
    message: str


class CuisineSubIn(BaseModel):
    cuisines: List


class CuisineMainOut(BaseModel):
    mainSummary: Dict
    cuisines: List


class CuisineSubOut(BaseModel):
    subSummary: Dict
    images: Dict


# above methods are for data creation from wikiscrape
# below methods are for posgreSQL interaction


class CuisineCreateIn(BaseModel):
    dayPlan_id: int
    country: str
    cuisines: Dict


class CuisineCreateOut(BaseModel):
    mainCuisine_id: int
    itinerary: Dict
    dayPlan_id: int
    country: str


class MainCuisineIn(BaseModel):
    dayPlan_id: int
    country: str
    mainSummary: str


class MainCuisineOut(BaseModel):
    id: int
    dayPlan_id: int
    country: str
    mainSummary: str


class SubCuisineIn(BaseModel):
    mainCuisine_id: int
    dayIndex: int
    name: str
    info: str
    summary: Optional[str]


class SubCuisineOut(BaseModel):
    id: int
    mainCuisine_id: int
    dayIndex: int
    name: str
    info: str
    summary: Optional[str]


class SubCuisineImageIn(BaseModel):
    subCuisine_id: int
    image: str


class SubCuisineImageOut(BaseModel):
    id: int
    subCuisine_id: int
    image: str


class GetAllCuisines(BaseModel):
    mainSummary: str
    itinerary: Dict


class CuisineQueries:
    def get_main(self, country: str) -> CuisineMainOut:
        pageId = get_pageId(country, "cuisine")
        cuisines = get_cuisineLinks(pageId)
        mainSummary = get_summary(page_id=pageId)
        return CuisineMainOut(cuisines=cuisines, mainSummary=mainSummary)

    def get_sub(self, info: CuisineSubIn) -> CuisineSubOut:
        subArray = [get_summary(page_title=item) for item in info.cuisines]
        subSummary = dict(ChainMap(*subArray))
        imgArray = [get_cuisine_imgLinks(item) for item in info.cuisines]
        imgSrc = {}
        for obj in imgArray:
            for cuisine, imgLinks in obj.items():
                imgSrc[cuisine] = get_imgSrc(imgLinks)
        return CuisineSubOut(subSummary=subSummary, images=imgSrc)

    def create_mainCuisine(self, cuisine: MainCuisineIn) -> MainCuisineOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                try:
                    result = db.execute(
                        """
                        INSERT INTO main_cuisine (
                            day_plan_id,
                            country,
                            summary
                        )
                        VALUES (%s, %s, %s)
                        RETURNING id
                        """,
                        [
                            cuisine.dayPlan_id,
                            cuisine.country,
                            cuisine.mainSummary,
                        ],
                    )
                    id = result.fetchone()[0]
                    cuisineIn = cuisine.dict()
                    return MainCuisineOut(id=id, **cuisineIn)
                except Exception as e:
                    raise e

    def create_subCuisine(self, cuisine: SubCuisineIn) -> SubCuisineOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                try:
                    result = db.execute(
                        """
                        INSERT INTO sub_cuisine(
                            main_cuisine_id,
                            day_index,
                            name,
                            info,
                            summary
                        )
                        VALUES (%s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            cuisine.mainCuisine_id,
                            cuisine.dayIndex,
                            cuisine.name,
                            cuisine.info,
                            cuisine.summary,
                        ],
                    )
                    id = result.fetchone()[0]
                    cuisineIn = cuisine.dict()
                    return SubCuisineOut(id=id, **cuisineIn)
                except Exception as e:
                    raise e

    def create_subCuisine_images(
        self, image: SubCuisineImageIn
    ) -> SubCuisineImageOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                try:
                    result = db.execute(
                        """
                        INSERT INTO subCuisine_image (
                            sub_cuisine_id,
                            image
                        )
                        VALUES (%s, %s)
                        RETURNING id;
                        """,
                        [image.subCuisine_id, image.image],
                    )
                    id = result.fetchone()[0]
                    imageIn = image.dict()
                    return SubCuisineImageOut(id=id, **imageIn)
                except Exception as e:
                    raise e

    def create_cuisine(self, startData: CuisineCreateIn) -> CuisineCreateOut:
        mainCuisine_data = MainCuisineIn(
            dayPlan_id=startData.dayPlan_id,
            country=startData.country,
            mainSummary=startData.cuisines.get("mainSummary"),
        )
        mainCuisine = self.create_mainCuisine(mainCuisine_data)
        # startPlanData.cuisine = {
        #     mainSummary:str,
        #     itinerary:{
        #     'dayIndex': [{
        #             info
        #             name
        #             summary
        #             images:[img]
        #         }]
        #     }
        # }
        itineraryIn = startData.cuisines.get("itinerary")
        itineraryOut = {}
        if itineraryIn:
            for day in itineraryIn:
                temp = []
                for obj in itineraryIn[day]:
                    subCuisine_data = SubCuisineIn(
                        mainCuisine_id=mainCuisine.id,
                        dayIndex=day,
                        name=obj.get("name"),
                        info=obj.get("info"),
                        sumary=obj.get("summary"),
                    )
                    subCuisine = self.create_subCuisine(subCuisine_data)
                    temp.append(subCuisine.id)
                    for img in obj.get("images"):
                        subCuisine_image_data = SubCuisineImageIn(
                            subCuisine_id=subCuisine.id, image=img
                        )
                        self.create_subCuisine_images(subCuisine_image_data)
                itineraryOut[day] = temp

        return CuisineCreateOut(
            mainCuisine_id=mainCuisine.id,
            itinerary=itineraryOut,
            dayPlan_id=startData.dayPlan_id,
            country=startData.country,
        )

    def get_mainCuisine(self, dayPlan_id: int) -> Union[MainCuisineOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                            mc.id,
                            mc.day_plan_id,
                            mc.country,
                            mc.summary
                        FROM main_cuisine as mc
                        WHERE mc.day_plan_id = %s
                        ORDER BY day_plan_id;
                        """,
                        [dayPlan_id],
                    )
                    row = result.fetchone()
                    return MainCuisineOut(
                        id=row[0],
                        dayPlan_id=row[1],
                        country=row[2],
                        mainSummary=row[3],
                    )

        except Exception as e:
            print(e)
            return {"message": f"{e}"}

    def get_subCuisine(
        self, mainCuisine_id: int
    ) -> Union[List[SubCuisineOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                            sc.id,
                            sc.main_cuisine_id,
                            sc.day_index,
                            sc.name,
                            sc.info,
                            sc.summary
                        FROM sub_cuisine as sc
                        WHERE sc.main_cuisine_id = %s
                        ORDER BY sc.day_index
                        """,
                        [mainCuisine_id],
                    )
                    rows = result.fetchall()
                    return [
                        SubCuisineOut(
                            id=row[0],
                            mainCuisine_id=row[1],
                            dayIndex=row[2],
                            name=row[3],
                            info=row[4],
                            summary=row[5],
                        )
                        for row in rows
                    ]
        except Exception as e:
            print(e)
            return {"message": f"{e}"}

    def get_subCuisine_images(
        self, sub_id: List[int]
    ) -> Union[List[str], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                            sci.id,
                            sci.sub_cuisine_id,
                            sci.image
                        FROM subCuisine_image as sci
                        WHERE sci.sub_cuisine_id = %s
                        ORDER BY sci.sub_cuisine_id
                        """,
                        [sub_id],
                    )
                    rows = result.fetchall()
                    return [
                        {
                            "id": row[0],
                            "subCuisine_id": row[1],
                            "image": row[2],
                        }
                        for row in rows
                    ]
        except Exception as e:
            print(e)
            return {"message": f"{e}"}

    def get_itinerary(self, sub_list: List[SubCuisineOut]):
        itinerary = {}
        for cuisine in sub_list:
            obj = {
                "name": cuisine.name,
                "info": cuisine.info,
                "summary": cuisine.summary,
            }
            obj["images"] = [
                item.image
                for item in self.get_subCuisine_images(cuisine.id)
                if hasattr(item, "image")
            ]
            if obj["images"] == []:
                del obj["images"]
            day = cuisine.dayIndex
            if itinerary.get(day):
                itinerary[day] = [*itinerary[day], obj]
            else:
                itinerary[day] = [obj]
        return itinerary

    def get_all(self, dayPlan_id: int) -> Union[GetAllCuisines, Error]:
        mainCuisine = self.get_mainCuisine(dayPlan_id)
        summary = ""
        summary = mainCuisine.mainSummary
        subCuisineList = self.get_subCuisine(mainCuisine.id)
        itineraryOut = {}
        if subCuisineList:
            itineraryOut = self.get_itinerary(subCuisineList)
        return GetAllCuisines(mainSummary=summary, itinerary=itineraryOut)
