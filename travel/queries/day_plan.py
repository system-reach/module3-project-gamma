from pydantic import BaseModel
from typing import Optional, List, Union, Dict
from datetime import date
from queries.pool import pool


class Error(BaseModel):
    message: str


class DayPlanIn(BaseModel):
    owner_id: int
    city: str
    state: Optional[str]
    country: str
    startDate: date
    endDate: date


class DayPlanOut(BaseModel):
    id: int
    owner_id: int
    city: str
    state: Optional[str]
    country: str
    startDate: date
    endDate: date


class StartDayPlanCreateIn(BaseModel):
    city: str
    state: Optional[str]
    country: str
    startDate: date
    endDate: date
    cuisines: Dict
    trails: Dict
    attractions: Dict


class StartDayPlanCreateOut(BaseModel):
    id: int
    owner_id: int
    city: str
    state: Optional[str]
    country: str
    startDate: date
    endDate: date
    cuisines: Dict
    trails: Dict
    attractions: Dict


class GetAllDayPlans(BaseModel):
    dayPlans: List[DayPlanOut]


class DayPlanRepository:
    def get_all(self, accountId: int) -> Union[Error, List[DayPlanOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                            day_plan.id,
                            day_plan.owner_id,
                            day_plan.city,
                            day_plan.state,
                            day_plan.country,
                            day_plan.startDate,
                            day_plan.endDate
                        FROM day_plan
                        WHERE day_plan.owner_id = %s
                        ORDER BY day_plan.id;
                        """,
                        [accountId],
                    )
                    rows = result.fetchall()
                    print(rows)
                    return [
                        {
                            "id": row[0],
                            "owner_id": row[1],
                            "city": row[2],
                            "state": row[3],
                            "country": row[4],
                            "startDate": row[5],
                            "endDate": row[6],
                        }
                        for row in rows
                    ]

        except Exception as e:
            print(e)
            return {"message": "The Day plan doesnt Exist"}

    def get_one(self, id: int) -> DayPlanOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT
                        day_plan.id,
                        day_plan.owner_id,
                        day_plan.city,
                        day_plan.state,
                        day_plan.country,
                        day_plan.startDate,
                        day_plan.endDate
                    FROM day_plan
                    WHERE day_plan.id = %s;
                    """,
                    [id],
                )
                row = result.fetchone()
                if row is None:
                    return None
                return DayPlanOut(
                    id=row[0],
                    owner_id=row[1],
                    city=row[2],
                    state=row[3],
                    country=row[4],
                    startDate=row[5],
                    endDate=row[6],
                )

    def create(self, day_plan: DayPlanIn) -> DayPlanOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO day_plan
                            (owner_id, city, state, country, startDate, endDate)
                        VALUES
                            (%s, %s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            day_plan.owner_id,
                            day_plan.city,
                            day_plan.state,
                            day_plan.country,
                            day_plan.startDate,
                            day_plan.endDate,
                        ],
                    )
                    id = result.fetchone()[0]
                    old_data = day_plan.dict()
                    return DayPlanOut(id=id, **old_data)

        except Exception as e:
            print(e)
            return {"message": "Could not set that day plan"}

    def delete(self, day_plan_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        DELETE FROM day_plan
                        WHERE id = %s
                        """,
                        [day_plan_id],
                    )
                    return True
        except Exception as e:
            return False
