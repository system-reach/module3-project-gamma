from pydantic import BaseModel
from typing import Optional
import os

OPEN_WEATHER_API_KEY = os.environ.get("OPEN_WEATHER_API_KEY")


class LocationIn(BaseModel):
    city: str
    state: Optional[str]
    country: str


class LocationOut(BaseModel):
    name: str
    lat: float
    lon: float


class WeatherOut(BaseModel):
    name: str
    main: str
    description: str
    temp: float
    feels_like: float
    humidity: float
