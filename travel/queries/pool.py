import os
from psycopg_pool import ConnectionPool


pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


# new connection pool address
#   DATABASE_URL: postgres://systemreact:dpE7R9XoLSgAfxUqtC8nA9txi4mN6N1U@dpg-ce9bkaun6mpgqu8lcf8g-a/postgresdata
# old connection pool address
#   "DATABASE_URL"
