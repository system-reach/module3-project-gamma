from pydantic import BaseModel
from typing import Optional, List, Union, Dict
from datetime import time
from queries.pool import pool


class Error(BaseModel):
    message: str


class TrailIn(BaseModel):
    day_plan_id: int
    day_index: int
    name: str
    info: str
    description: str
    length: str
    difficulty: str
    rating: int
    thumbnail: Optional[str]
    url: str
    time: Optional[time]


class TrailOut(BaseModel):
    id: int
    day_plan_id: int
    day_index: int
    name: str
    info: str
    description: str
    length: Optional[str]
    difficulty: Optional[str]
    rating: Optional[int]
    thumbnail: Optional[str]
    url: Optional[str]
    time: Optional[time]


class TrailRepository:
    def get_all(self, dayPlan_id: int) -> Union[Error, List[TrailOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                            trail_data.id,
                            trail_data.day_plan_id,
                            trail_data.day_index,
                            trail_data.name,
                            trail_data.info,
                            trail_data.description,
                            trail_data.length,
                            trail_data.difficulty,
                            trail_data.rating,
                            trail_data.thumbnail,
                            trail_data.url,
                            trail_data.time
                        FROM trail_data
                        WHERE trail_data.day_index = %s
                        Order by day_index;
                        """,
                        [dayPlan_id],
                    )
                    rows = result.fetchall()
                    return [
                        {
                            "id": row[0],
                            "day_plan_id": row[1],
                            "day_index": row[2],
                            "name": row[3],
                            "info": row[4],
                            "description": row[5],
                            "length": row[6],
                            "difficulty": row[7],
                            "rating": row[8],
                            "thumbnail": row[9],
                            "url": row[10],
                            "time": row[11],
                        }
                        for row in rows
                    ]

        except Exception as e:
            print(e)
            return {"message": "The list doesnt Exist"}

    def create(self, trail: TrailIn) -> TrailOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO trail_data (
                            day_plan_id,
                            day_index,
                            name,
                            info,
                            description,
                            length,
                            difficulty,
                            rating,
                            thumbnail,
                            url,
                            time
                        )
                        VALUES
                            (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            trail.day_plan_id,
                            trail.day_index,
                            trail.name,
                            trail.info,
                            trail.description,
                            trail.length,
                            trail.difficulty,
                            trail.rating,
                            trail.thumbnail,
                            trail.url,
                            trail.time,
                        ],
                    )
                    id = result.fetchone()[0]
                    old_data = trail.dict()
                return TrailOut(id=id, **old_data)
        except Exception as e:
            print(e)
            return {"message": "Could not set that trail"}

    def delete(self, trail_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        DELETE FROM trail_data
                        WHERE id = %s
                        """,
                        [trail_id],
                    )
                    return True
        except Exception as e:
            return False
