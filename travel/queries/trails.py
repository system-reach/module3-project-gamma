from pydantic import BaseModel
from typing import List
import os

TRAIL_API_KEY = os.environ["TRAIL_API_KEY"]


class TrailIn(BaseModel):
    lat: float
    lon: float
    per_page: int


class TrailInfoIn(BaseModel):
    id: int


class TrailOut(BaseModel):
    trails: List
