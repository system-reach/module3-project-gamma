from pydantic import BaseModel
from typing import Optional, List, Union
from datetime import date
from queries.pool import pool


class Error(BaseModel):
    message: str


class TripIn(BaseModel):
    owner_id: int
    start_date: date
    end_date: date
    country: str
    city: str
    state: Optional[str]


class TripOut(BaseModel):
    id: int
    owner_id: int
    start_date: date
    end_date: date
    country: str
    city: str
    state: Optional[str]


class TripRepository:
    def get_all(self) -> Union[Error, List[TripOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        Select id, owner_id, start_date, end_date, country, city, state
                        FROM trip_data
                        Order by start_date;
                        """
                    )
                    rows = result.fetchall()
                    # return TripOut(
                    #     id = result[0],
                    #     start_date= result[1],
                    #     end_date= result[2],
                    #     country= result[3],
                    #     city= result[4],
                    #     state= result[5]
                    # )
                    return [
                        {
                            "id": row[0],
                            "owner_id": row[1],
                            "start_date": row[2],
                            "end_date": row[3],
                            "country": row[4],
                            "city": row[5],
                            "state": row[6],
                        }
                        for row in rows
                    ]

        except Exception as e:
            print(e)
            return {"message": "The list doesnt Exist"}

    def get_all_id(self, owner_id: int) -> Union[Error, List[TripOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        Select
                            trip_data.id,
                            trip_data.owner_id,
                            trip_data.start_date,
                            trip_data.end_date,
                            trip_data.country,
                            trip_data.city,
                            trip_data.state
                        FROM trip_data
                        INNER JOIN accounts
                            ON (accounts.id = trip_data.owner_id)
                        WHERE trip_data.owner_id = %s;

                        """,
                        [owner_id],
                    )
                    rows = result.fetchall()
                    return [
                        {
                            "id": row[0],
                            "owner_id": row[1],
                            "start_date": row[2],
                            "end_date": row[3],
                            "country": row[4],
                            "city": row[5],
                            "state": row[6],
                        }
                        for row in rows
                    ]

        except Exception as e:
            print(e)
            return {"message": "The list doesn't Exist"}

    def create(self, trip: TripIn) -> TripOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO trip_data
                            (owner_id, start_date, end_date, country, city, state)
                        VALUES
                            (%s, %s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            trip.owner_id,
                            trip.start_date,
                            trip.end_date,
                            trip.country,
                            trip.city,
                            trip.state,
                        ],
                    )
                    id = result.fetchone()[0]
                    old_data = trip.dict()
                return TripOut(id=id, **old_data)
        except Exception as e:
            print(e)
            return {"message": "Could not set that trip"}

    def delete(self, trip_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        DELETE FROM trip_data
                        WHERE id = %s
                        """,
                        [trip_id],
                    )
                    return True
        except Exception as e:
            return False
