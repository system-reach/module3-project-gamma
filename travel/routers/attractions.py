from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)

# from queries.attractions import AttractionIn, AttractionQueries
from queries.attractions import AttractionIn, AttractionOut

# from queries.locations import LocationIn, LocationQueries
from typing import Union
from routers.locations import get_location
import requests
import json
import os

PLACES_API_KEY = os.environ.get("PLACES_API_KEY")

router = APIRouter()


@router.get("/api/xids")
def get_xids(city: str, country: str, state: Union[str, None] = None):
    location = get_location(city, country, state)
    lat = location.lat
    lon = location.lon
    querystring = {
        "radius": "16000",
        "lon": f"{lon}",
        "lat": f"{lat}",
        "kinds": "museums",
        "limit": "5",
    }
    headers = {
        "X-RapidAPI-Key": PLACES_API_KEY,
        "X-RapidAPI-Host": "opentripmap-places-v1.p.rapidapi.com",
    }
    url = "https://opentripmap-places-v1.p.rapidapi.com/en/places/radius"
    response = requests.get(url, headers=headers, params=querystring)
    content = json.loads(response.content)
    xids = []
    for i in range(5):
        xids.append(content["features"][i]["properties"]["xid"])
    return xids


@router.get("/api/attractions", response_model=AttractionOut)
def get_attractions(city: str, country: str, state: Union[str, None] = None):
    res = []
    xids = get_xids(city, country, state)
    for i in range(len(xids)):
        xid = xids[i]
        headers = {
            "X-RapidAPI-Key": PLACES_API_KEY,
            "X-RapidAPI-Host": "opentripmap-places-v1.p.rapidapi.com",
        }
        url = (
            f"https://opentripmap-places-v1.p.rapidapi.com/en/places/xid/{xid}"
        )
        response = requests.get(url, headers=headers)
        content = json.loads(response.content)
        obj = {}
        if content.get("name") != None:
            obj["name"] = content["name"]
        else:
            obj["name"] = ""
        if content["address"].get("house_number") != None:
            house_number = content["address"]["house_number"]
        else:
            house_number = ""
        if content["address"].get("road") != None:
            road = content["address"]["road"]
        else:
            road = ""
        if content["address"].get("city") != None:
            city = content["address"]["city"]
        else:
            city = ""
        if content["address"].get("state") != None:
            state = content["address"]["state"]
        else:
            state = ""
        if content["address"].get("postcode") != None:
            postcode = content["address"]["postcode"]
        else:
            postcode = ""
        if content["address"].get("country") != None:
            country = content["address"]["country"]
        else:
            country = ""
        obj[
            "address"
        ] = f"{house_number} {road}, {city}, {state} {postcode}, {country}"
        if content.get("wikipedia_extracts") != None:
            obj["description"] = content["wikipedia_extracts"]["text"]
        else:
            obj["description"] = "No description available"
        res.append(obj)
    return AttractionOut(attractions=res)
