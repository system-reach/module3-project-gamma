from fastapi import APIRouter, Depends, Response
from typing import List, Union
from queries.attractions_data import (
    Error,
    AttractionIn,
    AttractionsRepository,
    AttractionOut,
)

router = APIRouter()


@router.post("/attractions", response_model=Union[Error, AttractionOut])
def create_attractions(
    attractions: AttractionIn,
    response: Response,
    repo: AttractionsRepository = Depends(),
):
    return repo.create(attractions)


@router.get("/attractions", response_model=Union[Error, List[AttractionOut]])
def get_all(repo: AttractionsRepository = Depends()):
    return repo.get_all()


@router.delete("/attractions/{attractions_id}", response_model=bool)
def delete_attractions(
    attractions_id: int,
    repo: AttractionsRepository = Depends(),
) -> bool:
    return repo.delete(attractions_id)
