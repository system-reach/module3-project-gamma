from fastapi import Depends, APIRouter
from queries.countries import CountryQueries

router = APIRouter()


@router.get("/api/countries")
def get_countries(query: CountryQueries = Depends()):
    countries = query.get_countries()
    return countries
