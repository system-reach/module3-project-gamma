from fastapi import (
    Depends,
    HTTPException,
    status,
    APIRouter,
    Query,
)

from typing import List, Union
from queries.cuisines import (
    CuisineMainOut,
    CuisineSubIn,
    CuisineSubOut,
    CuisineQueries,
)

router = APIRouter()


@router.get("/api/cuisines/main", response_model=CuisineMainOut)
def get_main(country: str, query: CuisineQueries = Depends()):
    try:
        main = query.get_main(country)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=f"{e}"
        )
    return main


@router.get("/api/cuisines/sub/", response_model=CuisineSubOut)
def get_sub(
    cuisines: Union[List[str], None] = Query(default=None),
    query: CuisineQueries = Depends(),
):

    queryItems = {"cuisines": cuisines}
    print("GET_SUB ROUTEEEEEEEEEEEEEEE", cuisines)
    queryCuisine = CuisineSubIn(cuisines=cuisines)

    try:
        regional = query.get_sub(queryCuisine)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=f"{e}"
        )
    return regional
