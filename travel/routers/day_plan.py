from fastapi import APIRouter, Depends, Response, Request
from queries.day_plan import (
    StartDayPlanCreateIn,
    StartDayPlanCreateOut,
    GetAllDayPlans,
    DayPlanIn,
    DayPlanOut,
    DayPlanRepository,
    Error,
)
from queries.cuisines import CuisineQueries, CuisineCreateIn
from queries.trail_data import TrailRepository, TrailIn
from queries.attractions_data import AttractionsRepository, AttractionIn
from queries.accounts import AccountOut
from typing import Union, List, Optional
from authenticator import authenticator

router = APIRouter()


@router.post("/dayplan", response_model=Union[StartDayPlanCreateOut, Error])
def create_day_plan(
    startPlanData: StartDayPlanCreateIn,
    dayPlan_repo: DayPlanRepository = Depends(),
    cuisine_repo: CuisineQueries = Depends(),
    trail_repo: TrailRepository = Depends(),
    attractions_repo: AttractionsRepository = Depends(),
    account_data: AccountOut = Depends(
        authenticator.try_get_current_account_data
    ),
) -> StartDayPlanCreateOut:
    owner_id = account_data.get("id")

    dayPlan_data = DayPlanIn(
        owner_id=owner_id,
        city=startPlanData.city,
        state=startPlanData.state,
        country=startPlanData.country,
        startDate=startPlanData.startDate,
        endDate=startPlanData.endDate,
    )

    dayPlanOut_data = dayPlan_repo.create(dayPlan_data)

    # startPlanData.cuisine = {
    #     mainSummary:str,
    #     itinerary:{
    #     'dayIndex': [{
    #             info
    #             name
    #             summary
    #             images:[img]
    #         }]
    #     }
    # }
    cuisineOutData = {}
    if startPlanData.cuisines is not None:
        cuisineCreate_data = CuisineCreateIn(
            dayPlan_id=dayPlanOut_data.id,
            country=dayPlanOut_data.country,
            cuisines=startPlanData.cuisines,
        )
        cuisineOutData = cuisine_repo.create_cuisine(cuisineCreate_data)

    # startPlanData.trail = {
    #     'dayIndex': [{
    #         name,
    #         info,
    #         description,
    #         length,
    #         rating,
    #         difficulty,
    #         thumbnail,
    #         url,
    #         time
    #     }]
    # }
    trailOutData = {}
    if startPlanData.trails:
        for day in startPlanData.trails:
            temp = []
            for trail in startPlanData.trails[day]:
                trailCreate_data = TrailIn(
                    day_plan_id=dayPlanOut_data.id, day_index=day, **trail
                )
                trailOut = trail_repo.create(trailCreate_data)
                temp.append(trailOut.id)
        trailOutData[day] = temp

    attractionsOutData = {}
    if startPlanData.attractions:
        for day in startPlanData.attractions:
            temp = []
            for attractions in startPlanData.attractions[day]:
                attractionCreate_data = AttractionIn(
                    day_plan_id=dayPlanOut_data.id,
                    day_index=day,
                    **attractions
                )
                attractionsOut = attractions_repo.create(attractionCreate_data)
                temp.append(attractionsOut.id)
        attractionsOutData[day] = temp

    return StartDayPlanCreateOut(
        id=dayPlanOut_data.id,
        owner_id=dayPlanOut_data.owner_id,
        city=dayPlanOut_data.city,
        state=dayPlanOut_data.state,
        country=dayPlanOut_data.country,
        startDate=dayPlanOut_data.startDate,
        endDate=dayPlanOut_data.endDate,
        cuisines=cuisineOutData,
        trails=trailOutData,
        attractions=attractionsOutData,
    )


@router.get("/dayplan", response_model=Union[GetAllDayPlans, Error])
def get_all_day_plans(
    repo: DayPlanRepository = Depends(),
    account_data: AccountOut = Depends(
        authenticator.try_get_current_account_data
    ),
) -> DayPlanOut:
    owner_id = account_data.get("id")
    dayPlanList = repo.get_all(owner_id)
    return GetAllDayPlans(dayPlans=dayPlanList)


@router.get(
    "/dayplan/{day_plan_id}",
    response_model=Union[Error, StartDayPlanCreateOut],
)
def get_day_plan(
    day_plan_id: int,
    dayPlan_repo: DayPlanRepository = Depends(),
    cuisine_repo: CuisineQueries = Depends(),
    trail_repo: TrailRepository = Depends(),
    attractions_repo: AttractionsRepository = Depends(),
):
    dayPlan = dayPlan_repo.get_one(day_plan_id)
    cuisine = cuisine_repo.get_all(day_plan_id)
    trail = trail_repo.get_all(day_plan_id)
    attractions = attractions_repo.get_all(day_plan_id)
    return StartDayPlanCreateOut(
        **dayPlan.dict(), cuisine=cuisine, trail=trail, attractions=attractions
    )


@router.delete("/dayplan/{day_plan_id}", response_model=bool)
def delete_day_plan(
    day_plan_id: int,
    repo: DayPlanRepository = Depends(),
) -> bool:
    return repo.delete(day_plan_id)
