from fastapi import (
    HTTPException,
    status,
    APIRouter,
)
from queries.locations import (
    LocationOut,
    WeatherOut,
)
import os
import requests
import json
from typing import Union

OPEN_WEATHER_API_KEY = os.environ.get("OPEN_WEATHER_API_KEY")
router = APIRouter()


@router.get("/api/locations", response_model=LocationOut)
def get_location(city: str, country: str, state: Union[str, None] = None):
    try:
        if state is not None:
            params = {
                "q": f"{city},{state},{country}",
                "limit": 1,
                "appid": OPEN_WEATHER_API_KEY,
            }
        else:
            params = {
                "q": f"{city}, {country}",
                "limit": 1,
                "appid": OPEN_WEATHER_API_KEY,
            }
        url = "http://api.openweathermap.org/geo/1.0/direct"
        response = requests.get(url, params=params)
        content = json.loads(response.content)
        return LocationOut(
            name=content[0]["name"],
            lat=content[0]["lat"],
            lon=content[0]["lon"],
        )
    except (KeyError, IndexError):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Location Does Not Exist",
        )


@router.get("/api/weather", response_model=WeatherOut)
def get_weather(city: str, country: str, state: Union[str, None] = None):
    try:
        location = get_location(city, country, state)
        lat = location.lat
        lon = location.lon
        params = {
            "lat": lat,
            "lon": lon,
            "appid": OPEN_WEATHER_API_KEY,
            "units": "imperial",
        }
        url = "https://api.openweathermap.org/data/2.5/weather"
        response = requests.get(url, params=params)
        content = json.loads(response.content)
        return WeatherOut(
            name=content["name"],
            main=content["weather"][0]["main"],
            description=content["weather"][0]["description"],
            temp=content["main"]["temp"],
            feels_like=content["main"]["feels_like"],
            humidity=content["main"]["humidity"],
        )
    except (KeyError, IndexError):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Location Does Not Exist",
        )
