from fastapi import APIRouter, Depends, Response
from queries.trail_data import TrailIn, TrailRepository, TrailOut, Error
from typing import Union, List


router = APIRouter()


@router.post("/trails", response_model=Union[TrailOut, Error])
def create_trail(
    trail: TrailIn, response: Response, repo: TrailRepository = Depends()
):
    return repo.create(trail)


@router.get("/trails", response_model=Union[Error, List[TrailOut]])
def get_all(repo: TrailRepository = Depends()):
    return repo.get_all()


@router.delete("/trails/{trail_id}", response_model=bool)
def delele_trail(
    trail_id: int,
    repo: TrailRepository = Depends(),
) -> bool:
    return repo.delete(trail_id)
