from fastapi import APIRouter
from queries.trails import TrailOut
from typing import Union
from routers.locations import get_location
import requests
import json
import os

TRAIL_API_KEY = os.environ.get("TRAIL_API_KEY")

router = APIRouter()


@router.get("/api/trails", response_model=TrailOut)
def get_trail(city: str, country: str, state: Union[str, None] = None):
    location = get_location(city, country, state)
    lat = location.lat
    lon = location.lon
    querystring = {
        "lat": f"{lat}",
        "lon": f"{lon}",
        "per_page": "5",
        "radius": "100",
    }
    headers = {
        "X-RapidAPI-Key": TRAIL_API_KEY,
        "X-RapidAPI-Host": "trailapi-trailapi.p.rapidapi.com",
    }
    url = "https://trailapi-trailapi.p.rapidapi.com/trails/explore/"
    response = requests.get(url, headers=headers, params=querystring)
    contents = json.loads(response.content.decode("utf-8"))
    res = []
    if contents["results"] > 0:
        for i in range(contents["results"]):
            obj = {}
            obj["id"] = (contents["data"][i]["id"],)
            obj["name"] = (contents["data"][i]["name"],)
            obj["url"] = (contents["data"][i]["url"],)
            obj["length"] = (contents["data"][i]["length"],)
            obj["description"] = (contents["data"][i]["description"],)
            obj["difficulty"] = (contents["data"][i]["difficulty"],)
            obj["rating"] = (contents["data"][i]["rating"],)
            obj["thumbnail"] = (contents["data"][i]["thumbnail"],)
            res.append(obj)
    return TrailOut(trails=res)
