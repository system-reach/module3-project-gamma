from fastapi import APIRouter, Depends, Response
from authenticator import authenticator
from queries.trip_data import TripIn, TripRepository, TripOut, Error
from typing import Union, List


router = APIRouter()


@router.post("/trips", response_model=Union[TripOut, Error])
def create_trip(
    trip: TripIn, response: Response, repo: TripRepository = Depends()
):
    # response.status_code = 400
    return repo.create(trip)


@router.get("/trips", response_model=Union[Error, List[TripOut]])
def get_all(repo: TripRepository = Depends()):
    return repo.get_all()


@router.get("/trips/past", response_model=Union[Error, List[TripOut]])
def get_all_id(
    repo: TripRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    record = repo.get_all_id(account_data.get("id"))
    return record


@router.delete("/trips/{trip_id}", response_model=bool)
def delele_trip(
    trip_id: int,
    repo: TripRepository = Depends(),
) -> bool:
    return repo.delete(trip_id)
