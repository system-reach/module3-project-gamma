from queries.cuisines import (
    CuisineQueries,
    CuisineCreateIn,
    CuisineCreateOut,
    MainCuisineIn,
    MainCuisineOut,
    SubCuisineIn,
    SubCuisineOut,
    SubCuisineImageIn,
    SubCuisineImageOut,
)
from queries.accounts import AccountOut
from queries.trail_data import TrailIn, TrailOut
from queries.day_plan import DayPlanIn, DayPlanOut


class authenticatorMock:
    def try_get_current_account_data(self):
        return {
            "id": 10,
            "email": "mock@mock.com",
            "hashed_password": "mockPassword",
            "full_name": "mockFullName",
        }


class DayPlanRepositoryMock:
    def create(self, day_plan_data: DayPlanIn) -> DayPlanOut:
        return DayPlanOut(id=20, **day_plan_data.dict())


class CuisineQueriesMock:
    def create_mainCuisine(
        self, mainCuisine_data: MainCuisineIn
    ) -> MainCuisineOut:
        return MainCuisineOut(id=31, **mainCuisine_data.dict())

    def create_subCuisine(
        self, subCuisine_data: SubCuisineIn
    ) -> SubCuisineOut:
        return SubCuisineOut(id=32, **subCuisine_data.dict())

    def create_subCuisine_images(
        self, image_data: SubCuisineImageIn
    ) -> SubCuisineImageOut:
        return SubCuisineImageOut(id=33, **image_data.dict())

    def create_cuisine(
        self, createCuisineIn: CuisineCreateIn
    ) -> CuisineCreateOut:
        mainCuisine_data = MainCuisineIn(
            dayPlan_id=createCuisineIn.dayPlan_id,
            country=createCuisineIn.country,
            mainSummary=createCuisineIn.cuisines.get("mainSummary"),
        )
        mainCuisine = self.create_mainCuisine(mainCuisine_data)
        itineraryIn = createCuisineIn.cuisines.get("itinerary")
        itineraryOut = {}
        for day in itineraryIn:
            temp = []
            for obj in itineraryIn[day]:
                subCuisine_data = SubCuisineIn(
                    mainCuisine_id=mainCuisine.id,
                    dayIndex=day,
                    name=obj.get("name"),
                    info=obj.get("info"),
                    sumary=obj.get("summary"),
                )
                subCuisine = self.create_subCuisine(subCuisine_data)
                temp.append(subCuisine.id)
            itineraryOut[day] = temp
        return CuisineCreateOut(
            mainCuisine_id=mainCuisine.id,
            itinerary=itineraryOut,
            dayPlan_id=createCuisineIn.dayPlan_id,
            country=createCuisineIn.country,
        )


class TrailRepositoryMock:
    def create(self, trail_data: TrailIn) -> TrailOut:
        return TrailOut(id=40, **trail_data.dict())
