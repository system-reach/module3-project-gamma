import json
from fastapi.testclient import TestClient
from main import app
from queries.attractions_data import AttractionsRepository

client = TestClient(app)


class AttractionQueriesMock:
    def get_all(self):
        return []


def test_get_attractions():
    app.dependency_overrides[AttractionsRepository] = AttractionQueriesMock

    response = client.get("/attractions")

    assert response.status_code == 200

    assert response.json() == []

    app.dependency_overrides = {}
