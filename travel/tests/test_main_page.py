import json
from fastapi.testclient import TestClient
from main import app
from queries.trip_data import TripRepository


client = TestClient(app)  # replacing swagger / fast api docs


class TripQueriesMock:
    def get_all(self):
        return []

    def create(self, trip):
        response = {
            "id": 23,
            "account": {
                "id": 1,
                "email": "test@gmail.com",
                "password": "password",
                "full_name": "Tester test",
            },
        }
        response.update(trip)
        return response


def test_get_trips():
    # arrange
    app.dependency_overrides[TripRepository] = TripQueriesMock
    # act
    response = client.get("/trips")

    # assert
    # 1. get a 200
    assert response.status_code == 200
    # 2. should *call* get trip data
    assert response.json() == []

    # cleanup
    app.dependency_overrides = {}


def test_create_trip():
    # arrange
    app.dependency_overrides[TripRepository] = TripQueriesMock
    trip = {
        "owner_id": 1,
        "start_date": "2022-10-29",
        "end_date": "2022-12-10",
        "country": "USA",
        "city": "Brooklyn",
        "state": "New York",
    }
    # act
    response = client.post("/trips", json.dumps(trip))

    # assert
    assert response.status_code == 200

    assert response.json()["start_date"] == "2022-10-29"
