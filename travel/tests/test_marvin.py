import json
from fastapi.testclient import TestClient
from main import app
from queries.accounts import AccountQueries
from queries.countries import CountryQueries


client = TestClient(app)


class AccountQueriesMock:
    def get_all(self):
        return []


def test_accounts_list():
    app.dependency_overrides[AccountQueries] = AccountQueriesMock
    response = client.get("/api/accounts")
    assert response.status_code == 200
    assert response.json() == {"accounts": []}


class CountryQueriesMock:
    def get_countries(self):
        return []


# Working
def test_get_countries():
    app.dependency_overrides[CountryQueries] = CountryQueriesMock
    response = client.get("/api/countries")
    assert response.status_code == 200
    assert response.json() == []
