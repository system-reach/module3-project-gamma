import json
from unittest import TestCase
from datetime import date
from fastapi.testclient import TestClient
from main import app
from queries.accounts import AccountOut
from queries.day_plan import DayPlanRepository, StartDayPlanCreateIn
from queries.cuisines import CuisineQueries, CuisineCreateIn
from authenticator import authenticator
from queries.trail_data import TrailRepository
from mock_depends_sophia import (
    authenticatorMock,
    DayPlanRepositoryMock,
    CuisineQueriesMock,
    TrailRepositoryMock,
)

client = TestClient(app)


class test_cuisine_queries(TestCase, CuisineQueriesMock):
    def test_create_cuisine(self):
        # Arrange
        input = CuisineCreateIn(
            dayPlan_id=10,
            country="testCountry",
            cuisines={
                "itinerary": {
                    "0": [
                        {
                            "name": "testSubCuisine",
                            "info": "testSubInfo",
                            "summary": "testSubSummary",
                            "images": ["http//test.imageURL"],
                        }
                    ]
                },
                "mainSummary": "testMainSummary",
            },
        )

        expected = {
            "mainCuisine_id": 31,
            "itinerary": {"0": [32]},
            "dayPlan_id": 10,
            "country": "testCountry",
        }

        # Act
        output = self.create_cuisine(input)
        print(output.dict())
        # Assert


#         self.assertDictEqual(expected, output.dict())


# def test_create_dayPlan():
#     # arrange
#     # return AccountOut(
#     #     id=10,
#     #     email='mock@mock.com',
#     #     hashed_password='mockPassword',
#     #     full_name='mockFullName'
#     # )
#     app.dependency_overrides[
#         authenticator.try_get_current_account_data
#     ] = authenticatorMock.try_get_current_account_data
#     app.dependency_overrides[DayPlanRepository] = DayPlanRepositoryMock
#     app.dependency_overrides[CuisineQueries] = CuisineQueriesMock
#     app.dependency_overrides[TrailRepository] = TrailRepositoryMock

#     data = {
#         "city": "mockCity",
#         "state": "mockState",
#         "country": "mockCountry",
#         "startDate": "2022-12-12",
#         "endDate": "2022-12-12",
#         "cuisine": {
#             "mainSummary": "mockMainSummary",
#             "itinerary": {
#                 "0": [
#                     {
#                         "name": "mockSubCuisine",
#                         "info": "mockSubInfo",
#                         "summary": "mockSubSummary",
#                         "images": ["mock.imageURL"],
#                     }
#                 ]
#             },
#         },
#         "trail": {
#             "0": [
#                 {
#                     "description": "mockDescription",
#                     "difficulty": "mockDifficulty",
#                     "info": "mockInfo",
#                     "length": "mockLength",
#                     "name": "mockName",
#                     "rating": 0,
#                     "thumbnail": "mockThumbnail",
#                     "url": "mockURL",
#                 }
#             ]
#         },
#     }
#     expected = {
#         "id": 20,
#         "owner_id": 10,
#         "city": "mockCity",
#         "state": "mockState",
#         "country": "mockCountry",
#         "startDate": "2022-12-12",
#         "endDate": "2022-12-12",
#         "cuisine": {"mainSummary": 31, "itinerary": {"0": [32]}},
#         "trail": {"0": [40]},
#     }

#     # act
#     print(input)
#     response = client.post("/dayplan", json.dumps(data))
#     # assert
#     assert response.status_code == 200
#     output = response.json()
#     print(output)
#     TestCase().assertDictEqual(expected, output)

#     app.dependency_ovverrides = {}
